# URI Schema

... declares how an URI should be structured.


### JSON Example

URI Element
```json
  {
    "uri": {
      "hostname": "example.org",
      "path": "foo/bar"
    },
    "ssl": true,
    "www": false,
    "trailing_slash": false,
    "code": 301,
    "subdomain": false
  }
```

URI Build Config
```json
  "<scope-id>": {
    "ssl": true,
    "www": false,
    "trailing_slash": false,
    "code": 301,
    "subdomain": false
  }
```

An URI element is a information for [redirections](redirecting.md), [tagged URIs](tagged-uris.md), [executions](executing.md) and test values.

An URI build config is a build schema for a special domain, and is used as configuration for building URIs that are from this scope. A scope-id is a fully qualified domain name (FQDN), without the trailing dot! Build configs doesn't have a `uri` property.

Properties:

- `uri`:`object` The URI parts, in different other configuration this is e.g. `to` in redirect 
    - `hostname`:`string` the hostname, must be without a www subdomain, without a trailing slash, no protocol 
    - `path`:`string` the path of the uri, must not start or end with a slash
- `ssl`:`bool` when SSL is on (`true`) the protocol will be `https`, otherwise `http`
- `www`:`bool` when www is on (`true`) the hostname will have the `www` subdomain prefixed
- `trailing_slash`:`bool` when trailing slash is on (`true`) the path will end with a slash, also the param would be when set end with a slash
- `code`:`int` what header code should be used for redirections
- `subdomain`:`bool` if the domain should be treated like a subdomain (`true`), subdomains will not get a www prefixed, even when 

Informations about the object [UriSchema](../docs/uri-schema.json) could be helpful if you want to do further things with the [results](guide/integrate/results.json).