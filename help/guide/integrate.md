# Integrate

When you want to integrate MiniRoute into your existing application, there of cause are different ways to do so, this is one easy copy + paste way:

## Setting up mod_rewrite

MiniRoute depends on mod_rewrite to get all URIs.

Your application mostly have already a `.htaccess` file, if not create one.

When it already have one, you must search for lines that look like the following. For security reasons you should exclude the `config` folder from access like below.

The important part is where your application pushes requests (maybe all) to one file. We will further on call it "execution file".

When it doesn't have, add all lines below.
  
```apacheconf
<IfModule mod_rewrite.c>
    # for GET parameters
    Options +FollowSymlinks

    # should be utf-8
    AddCharset utf-8 .html .php .css .js

    RewriteEngine On

    # exclude some folder from rewrite
    RewriteRule ^folder_name/.*$ - [NC,L]

    # exclude some file from rewrite
    RewriteRule ^view/miniroute-logo-min.png$ - [L]

    # exclude from access
    RewriteRule ^config/.*$ - [NC,F]
    RewriteRule ^tmp/.*$ - [NC,F]

    # push everything into one file
    RewriteRule ^(.*)$ execution.php [NC,QSA,L]

</IfModule>
```

For security reasons we recommend to not rely on rule conditions with *do not rewrite when file exists* content. Please only exclude the folders and files from rewrite which doesn't need to.

When you found the execution file, open it. If your application doesn't use something like that, it could be difficult to integrate MiniRoute, we would be glad to [support](https://help.bemit.eu/project-support) you in this.

## Execution File

If you installed MiniRoute through composer, add if not already exists, this line to the top of the file:
```php
<?php
    require_once 'vendor/autoload.php';
```

If manually installed:
```php
<?php
    require_once 'miniroute/autoload.php';
```

That you could use the simple use `MiniRoute` as class name add those lines:  

```php
<?php
    use Bemit\MiniRoute\MiniRoute;
    // Usage of monitor is optinal
    use Flood\Component\PerformanceMonitor\Monitor;
```

Initialize a runtime of MiniRoute:

```php
<?php
    $mini_route = new MiniRoute();
```

Activate the cache, if the folder doesn't exist, MiniRoute creates it.

```php
<?php
    $mini_route->initCache([
        'path'       => 'tmp/miniroute',
        'on'         => true,
        'check_time' => true,
    ]);
```

#### Setup Environment Variables

##### Runtime Path

When you install MiniRoute in a subdirectory, and *all* URIs will have that subdirectory in their URI, you can add a RuntimePath, so you doen't need to write that folder in every routing.

```php
<?php
    $mini_route->addRuntimePath('miniroute');
```

##### Active Hostname, Path and Parameters

MiniRoute needs informations about the active URI, the hostname and the path with get parameters. Those are added to the runtime with `addUriActive()`, it will automatic set the active information from the `INPUT_SERVER` when not submitted.  

```php
<?php
    $mini_route->addUriActive();
    
    $mini_route->addUriActive([
        'hostname'   => filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_URL),
        'path-query' => filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL),
    ]);
```

##### URI Collection

The URI collection consists of saved URIs for a easier use when you want to redirect many URIs to one, want to build an URI with [UriBuild](../docs/uri-build.md) or other things.

The usage is optional.

```php
<?php
    $mini_route->addUriCollection('config/uri.json');
```

##### URI Build Config

The URI build config consists of informations about how the URIs should be build and what URIs are valid. The build config for one element is a [UriSchema](../docs/uri-schema.md).

```php
<?php
    $mini_route->addUriBuildConfig('config/uri-build.json');
```

##### Redirections

The redirect informations are used to determine if the active URI should be redirected to another. The structure of the JSON is explained [here](routing/redirecting.md).

```php
<?php
    $mini_route->addRedirect('config/redirect.json');
```

#### The Final Execution

Now let MiniRoute evaluated all input values and do whatever need to be done.

```php
<?php
    $mini_route->route();
    // activated debug, no redirection is done
    $mini_route->route(true);
```

If you doesn't use the [executor](routing/executing.md), from below here everything that your normal application does should be pasted. 