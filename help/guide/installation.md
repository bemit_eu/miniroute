# Installation

It is recommended to use [composer](https://getcomposer.org/download/) for installation: 

If you want to use MiniRoute with your existing application:
    
    composer require bemit/miniroute

When needed as standalone, use `create-project`:
    
    composer create-project bemit/miniroute
    
If it is not possible to use composer in your setup, download the archive of the latest version from [bitbucket](https://bitbucket.org/bemit_eu/miniroute/downloads/?tab=tags) and put the files into your project. 

MiniRoute is only tested on linux web server.

Minimum PHP Version is 5.6 but recommend is 7.1, if a feature needed does need 7.1 we doen't hesitate to unsupport 5.6.

MiniRoute comes bundled with [FloodComponent: PerformanceMonitor](https://bitbucket.org/bemit_eu/floodcomponent-performancemonitor).

## See also 
- How to [integrate](integrate.md) MiniRoute in your application.
- How to use MiniRoute as [standalone](standalone.md). (TODO)