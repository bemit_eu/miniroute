# MiniRoute Guide + Docs
Find help in the guide when you got usage problems and the docs for source code informations and examples. 

    todo: create content

## Guide

- [Installation](guide/installation.md)
- [Integrate](guide/integrate.md)
- [Routing](guide/routing.md)
    - [URI Schema](guide/routing/uri-schema.md)
    - [Tagged URIs](guide/routing/tagged-uris.md)
    - [Checking](guide/routing/checking.md)
    - [Redirecting](guide/routing/redirecting.md)
    - [Executing](guide/routing/executing.md)
    - [Results](guide/routing/results.md)
    
## Docs

- [MiniRoute](docs/mini-route.md)
- [UriSchema](docs/uri-schema.md)
- [UriBuild](docs/uri-build.md)
- [ResultChecker](docs/result-checker.md)
- [ResultRedirector](docs/result-redirector.md)

### Licence

Copyright 2017 Michael Becker

For the Licence applied on source code and documentation see [licence](../LICENCE.md).