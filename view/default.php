<?php

include 'layout/head.php';
include 'layout/logo.php';
include 'layout/footer.php';

use Bemit\MiniRoute\MiniRoute;
use Flood\Component\PerformanceMonitor\Monitor;

if(!isset($mini_route)) {
    // for ide autocomplete in this file
    $mini_route = new MiniRoute();
}

?>
<!DOCTYPE html>
<html>
<head>
    <?php defaultHead(['title' => 'MiniRoute default']); ?>
</head>
<body>
<div class="container-outer">
    <?php defaultLogo(); ?>
    <div class="container content">
        <h1>MiniRoute Default Page</h1>
        <table>
            <tr>
                <th colspan="3" class="h3">Active URI Informations</th>
            </tr>
            <tr>
                <td>Hostname</td>
                <td><?= $mini_route->getUri()->getActiveHostname(false) ?></td>
            </tr>
            <tr>
                <td>Path</td>
                <td><?= $mini_route->getUri()->getActivePath(false) ?></td>
            </tr>
            <tr>
                <td>Param</td>
                <td>(TODO)<?= $mini_route->getUri()->getActiveParam(false) ?></td>
            </tr>
            <tr>
                <td>SSL</td>
                <td><?= ($mini_route->getUri()->getActive()['info']['ssl'] ? 'on' : 'off') ?></td>
            </tr>
            <tr>
                <td>WWW</td>
                <td><?= ($mini_route->getUri()->getActive()['info']['www'] ? 'on' : 'off') ?></td>
            </tr>
            <tr>
                <td>Trailing Slash</td>
                <td><?= ($mini_route->getUri()->getActive()['info']['trailing_slash'] ? 'on' : 'off') ?></td>
            </tr>
            <tr>
                <td>Referer</td>
                <td><?= (isset($_SERVER['HTTP_REFERER']) ? filter_input(INPUT_SERVER, 'HTTP_REFERER', FILTER_SANITIZE_URL) : '-') ?></td>
            </tr>
        </table>

        <div class="redirect-info">
            <h3 class="h3">Redirector Result</h3>
            <p>Should the URI be redirected?</p>
            <?php
            if($mini_route->getRedirectResult()->getDo()) {
                ?>
                <p><b>Yes</b>, it should be done.</p>
                <p>The target will be
                    <a href="<?= $mini_route->getRedirectResult()->getToUriSchema()->getBuildedUri() ?>" target="_blank"><code><?= $mini_route->getRedirectResult()
                                ->getToUriSchema()->getBuildedUri() ?></code></a> with header code
                    <code><?= $mini_route->getRedirectResult()->getCode() ?></code></p>
                <?php
            } else {
                echo '<p><b>No</b>, nothing to do.</p>';
            }
            ?>
        </div>

        <table>
            <tr>
                <th colspan="2" class="h3">Check Result Informations</th>
            </tr>
            <tr>
                <th>Item</th>
                <th>Old</th>
                <th>New</th>
            </tr>
            <tr>
                <td>Hostname</td>
                <td><?= $mini_route->getCheckResult()->getOld()->getUri()['hostname'] ?></td>
                <td><?= $mini_route->getCheckResult()->getNew()->getUri()['hostname'] ?></td>
            </tr>
            <tr>
                <td>Path</td>
                <td><?= $mini_route->getCheckResult()->getOld()->getUri()['path'] ?></td>
                <td><?= $mini_route->getCheckResult()->getNew()->getUri()['path'] ?></td>
            </tr>
            <tr>
                <td>Param</td>
                <td>(TODO)<?php
                    if(isset($mini_route->getCheckResult()->getOld()->getUri()['param'])) {
                        $mini_route->getCheckResult()->getOld()->getUri()['param'];
                    }
                    ?></td>
                <td>(TODO)<?php
                    if(isset($mini_route->getCheckResult()->getNew()->getUri()['param'])) {
                        $mini_route->getCheckResult()->getNew()->getUri()['param'];
                    }
                    ?></td>
            </tr>
            <tr>
                <td>SSL</td>
                <td><?= ($mini_route->getCheckResult()->getOld()->getSsl() ? 'on' : 'off') ?></td>
                <td
                    class="<?= ($mini_route->getCheckResult()->getNew()->getSsl() !== $mini_route->getCheckResult()
                        ->getOld()
                        ->getSsl() ? 'alternate' : '') ?> <?= ($mini_route->getCheckResult()->getNew()->getSsl() ? 'on' : 'off') ?>"
                ><?= ($mini_route->getCheckResult()->getNew()->getSsl() ? 'on' : 'off') ?></td>
            </tr>
            <tr>
                <td>Trailing Slash</td>
                <td><?= ($mini_route->getCheckResult()->getOld()->getTrailingSlash() ? 'on' : 'off') ?></td>
                <td
                    class="<?= ($mini_route->getCheckResult()->getNew()->getTrailingSlash() !== $mini_route->getCheckResult()
                        ->getOld()
                        ->getTrailingSlash() ? 'alternate' : '') ?> <?= ($mini_route->getCheckResult()
                        ->getNew()
                        ->getTrailingSlash() ? 'on' : 'off') ?>"
                ><?= ($mini_route->getCheckResult()->getNew()->getTrailingSlash() ? 'on' : 'off') ?></td>
            </tr>
            <tr>
                <td>WWW</td>
                <td><?= ($mini_route->getCheckResult()->getOld()->getWww() ? 'on' : 'off') ?></td>
                <td
                    class="<?= ($mini_route->getCheckResult()->getNew()->getWww() !== $mini_route->getCheckResult()
                        ->getOld()
                        ->getWww() ? 'alternate' : '') ?> <?= ($mini_route->getCheckResult()->getNew()->getWww() ? 'on' : 'off') ?>"
                ><?= ($mini_route->getCheckResult()->getNew()->getWww() ? 'on' : 'off') ?></td>
            </tr>
            <tr>
                <td>Subdomain</td>
                <td></td>
                <td><?= ($mini_route->getCheckResult()->getNew()->getSubdomain() ? 'on' : 'off') ?></td>
            </tr>
            <tr>
                <td>Generated URI</td>
                <td colspan="2"><?= ($mini_route->getCheckResult()->getNew()->getBuildedUri()) ?></td>
            </tr>
        </table>
        <?php
        if($mini_route->getCheckResult()->getMismatch()) {
            ?><p><b>Check will redirect the URI.</b></p><?php
        } else {
            ?><p><b>Check passed.</b></p><?php
        }
        ?>

        <table>
            <tr>
                <th colspan="2" class="h3">URI Schema Informations</th>
            </tr>
            <tr>
                <th>Item</th>
                <th>General</th>
                <th>Scope</th>
            </tr>
            <tr>
                <td>SSL</td>
                <td class="<?= ($mini_route->getUriBuild()->getConfig('ssl') ? 'on' : 'off') ?> alternate"><?= ($mini_route->getUriBuild()
                        ->getConfig('ssl') ? 'on' : 'off') ?></td>
                <td class="<?= ($mini_route->getUriBuild()->getConfig('ssl', $mini_route->getUri()
                    ->getActiveHostname(false)) ? 'on' : 'off') ?> alternate"><?= ($mini_route->getUriBuild()->getConfig('ssl', $mini_route->getUri()
                        ->getActiveHostname(false)) ? 'on' : 'off') ?></td>
            </tr>
            <tr>
                <td>WWW</td>
                <td class="<?= ($mini_route->getUriBuild()->getConfig('www') ? 'on' : 'off') ?> alternate"><?= ($mini_route->getUriBuild()
                        ->getConfig('www') ? 'on' : 'off') ?></td>
                <td class="<?= ($mini_route->getUriBuild()->getConfig('www', $mini_route->getUri()
                    ->getActiveHostname(false)) ? 'on' : 'off') ?> alternate"><?= ($mini_route->getUriBuild()->getConfig('www', $mini_route->getUri()
                        ->getActiveHostname(false)) ? 'on' : 'off') ?></td>
            </tr>
            <tr>
                <td>Trailing Slash</td>
                <td class="<?= ($mini_route->getUriBuild()->getConfig('trailing_slash') ? 'on' : 'off') ?> alternate"><?= ($mini_route->getUriBuild()
                        ->getConfig('trailing_slash') ? 'on' : 'off') ?></td>
                <td class="<?= ($mini_route->getUriBuild()->getConfig('trailing_slash', $mini_route->getUri()
                    ->getActiveHostname(false)) ? 'on' : 'off') ?> alternate"><?= ($mini_route->getUriBuild()
                        ->getConfig('trailing_slash', $mini_route->getUri()
                            ->getActiveHostname(false)) ? 'on' : 'off') ?></td>
            </tr>
            <tr>
                <td>Code</td>
                <td><?= $mini_route->getUriBuild()->getConfig('code') ?></td>
                <td
                    class="<?= ($mini_route->getUriBuild()->getConfig('code', $mini_route->getUri()
                        ->getActiveHostname(false)) !== $mini_route->getUriBuild()->getConfig('code') ? 'alternate' : '') ?>"
                ><?= $mini_route->getUriBuild()->getConfig('code', $mini_route->getUri()->getActiveHostname(false)) ?></td>
            </tr>
            <tr>
                <td>Subdomain</td>
                <td class="<?= ($mini_route->getUriBuild()->getConfig('subdomain') ? 'on' : 'off') ?> alternate"><?= ($mini_route->getUriBuild()
                        ->getConfig('subdomain') ? 'on' : 'off') ?></td>
                <td class="<?= ($mini_route->getUriBuild()->getConfig('subdomain', $mini_route->getUri()
                    ->getActiveHostname(false)) ? 'on' : 'off') ?> alternate"><?= ($mini_route->getUriBuild()
                        ->getConfig('subdomain', $mini_route->getUri()
                            ->getActiveHostname(false)) ? 'on' : 'off') ?></td>
            </tr>
        </table>

        <div class="performance-monitor">
            <h3 class="h3">Performance</h3>
            <p>Performance Monitor records some profiles, attached in flow.php, to better analyse some performance aspects. The rendering of default.php is not beeing included. The calculated memory could be slightly different than the actual consumed memory.</p>
            <p>
                <code>0.0001s</code> is the smallest amount of time that could be displayed correct, meaning the logic runtime is perhabs shorter than that.
            </p>
            <?php
            $performance_result = Monitor::i()->getInformation();
            ?>
            <table>
                <tr>
                    <td></td>
                    <td>Time</td>
                    <td>Memory</td>
                </tr>
                <?php
                foreach($performance_result as $profile_id => $result) {
                    ?>
                    <tr>
                        <td><?= $profile_id ?></td>
                        <td><?= $result['time'] ?>s</td>
                        <td><?= Monitor::i()->convertMemory($result['memory']) ?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
        <div class='build-info'>
            <p>The Build Status for the last release could be viewed at:
                <a href="https://tools.bemit.eu/miniroute/miniroute.test">tools.bemit.eu/miniroute/miniroute.test</a>
            </p>
        </div>
    </div>

    <div class="footer-wrapper">
        <div class="container">
            <?php defaultFooter(); ?>
        </div>
    </div>
</div>
</body>
</html>
