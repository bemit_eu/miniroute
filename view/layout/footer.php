<?php

function defaultFooter() {
    ?>
    <div class="footer">
        <span><a href="https://bitbucket.org/bemit_eu/miniroute" target="_blank" class="to-bitbucket" title="MiniRoute bitbucket repository"><i class="fa fa-bitbucket" aria-hidden="true"></i></a> | <a href="https://packagist.org/packages/bemit/miniroute" target="_blank">composer <span class="hide-xs">require bemit/miniroute</span></a> | &copy; 2017 Michael Becker</span>
    </div>
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <script type="text/javascript"><?php echo str_replace(['  '], [''], preg_replace("/\r|\n/", '', file_get_contents(__DIR__ . '/../script.js'))); ?></script>
    <?php
}