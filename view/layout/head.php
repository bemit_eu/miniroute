<?php

function defaultHead($content) {
    ?>
    <title><?= $content['title'] ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style><?php echo str_replace(['  ', ' {', ': '], [
            '',
            '{',
            ':',
        ], preg_replace("/\r|\n/", '', file_get_contents(__DIR__ . '/../style.css'))); ?></style>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="https://tools.bemit.eu/miniroute/view/miniroute-logo-min.png"/>
    <?php
}