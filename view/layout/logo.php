<?php

function defaultLogo() {
    ?>
    <div class="logo-wrapper">
        <img src="https://tools.bemit.eu/miniroute/view/miniroute-logo-min.png" class="logo" alt="Logo of MiniRoute, two arrows which are going together and builds a larger arrow"/>
    </div>
    <?php
}