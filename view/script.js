$(function() {
    $('.tested-category a').click(function() {
        if($($($($(this).data('toggle'))).data('toggle')).is(':hidden')) {
            $($($($(this).data('toggle'))).data('toggle')).slideDown().removeClass('hidden').addClass('visible');
            $(this.parentElement).removeClass('hidden').addClass('visible');
        } else {
            $($($($(this).data('toggle'))).data('toggle')).slideUp().removeClass('visible').addClass('hidden');
            $(this.parentElement).removeClass('visible').addClass('hidden');
        }
        return true;
    });

    $('[data-click=on]').click(function() {
        if($($(this).data('toggle')).is(':hidden')) {
            $($(this).data('toggle')).slideDown().removeClass('hidden').addClass('visible');
            $(this.parentElement).removeClass('hidden').addClass('visible');
        } else {
            $($(this).data('toggle')).slideUp().removeClass('visible').addClass('hidden');
            $(this.parentElement).removeClass('visible').addClass('hidden');
        }
        return false;
    });
});