<?php

namespace Bemit\MiniRouteTest;

/**
 * Container for the result of Emulator
 *
 * @category
 * @package    \Bemit\MiniRoute
 * @author     Original Author mb@project.bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0.2.0
 * @version    0.0.1
 */

class ResultEmulator {

    /**
     * @var \Bemit\MiniRoute\ResultRedirector|null
     */
    protected $result_redirect;

    /**
     * @var \Bemit\MiniRoute\ResultChecker|null
     */
    protected $result_check;

    /**
     * @var string
     */
    protected $runtime_path;

    /**
     * @var \Bemit\MiniRoute\UriSchema|null
     */
    protected $active;

    public function __construct() {
        $this->result_redirect = null;

        $this->result_check = null;

        $this->runtime_path = '';

        $this->active = null;
    }

    /**
     * @param \Bemit\MiniRoute\UriSchema $active
     */
    public function setActive($active) {
        $this->active = $active;
    }

    /**
     * @return \Bemit\MiniRoute\UriSchema with indices 'hostname' and 'path'
     */
    public function getActive() {
        return $this->active;
    }

    /**
     * @param $result_redirect
     */
    public function setResultRedirect($result_redirect) {
        $this->result_redirect = $result_redirect;
    }

    /**
     * @return \Bemit\MiniRoute\ResultRedirector|null
     */
    public function getResultRedirect() {
        return $this->result_redirect;
    }

    /**
     * @param $result_check
     */
    public function setResultCheck($result_check) {
        $this->result_check = $result_check;
    }

    /**
     * @return \Bemit\MiniRoute\ResultChecker|null
     */
    public function getResultCheck() {
        return $this->result_check;
    }

    /**
     * @param $runtime_path
     */
    public function setRuntimePath($runtime_path) {
        $this->runtime_path = $runtime_path;
    }

    /**
     * @return string
     */
    public function getRuntimePath() {
        return $this->runtime_path;
    }
}