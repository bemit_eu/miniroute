<?php

namespace Bemit\MiniRouteTest;

//use \Bemit\MiniRoute\MiniRoute;
use Bemit\MiniRoute\Cache;

/**
 * MiniRouteTest Runtime Container
 *
 * @category
 * @package    \Bemit\MiniRouteTest
 * @author     Original Author mb@project.bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0.1.0
 * @version    0.1.1
 */
class MiniRouteTest {

    /**
     * @var \Bemit\MiniRouteTest\Emulator
     */
    public $emulator;

    /**
     * @var \Bemit\MiniRouteTest\BinaryTrueness
     */
    public $binary_trueness;

    /**
     * @var array [ '<cat_id>' => ['<id>' =>  ] ]
     */
    public $test_result;

    /**
     * Just for fetching json data
     *
     * @var \Bemit\MiniRoute\Cache
     */
    protected $cache;

    /**
     * MiniRouteTest constructor.
     */
    public function __construct() {
        $this->cache = new Cache();
        $this->cache->init(['on' => false]);

        $this->emulator = new Emulator();
        $this->binary_trueness = new BinaryTrueness();
    }

    /**
     * Fetches all test values and runs the emulator with correspondending config possibilities
     * sets the informations in test_result
     */
    public function run() {
        $test_list = [
            [
                'cat_id'            => 'path',
                'test_type'         => 'check',
                'with_runtime_path' => false,
            ],
            [
                'cat_id'            => 'path-w-runtimepath',
                'test_type'         => 'check',
                'with_runtime_path' => true,
            ],
            [
                'cat_id'            => 'domain',
                'test_type'         => 'check',
                'with_runtime_path' => false,
            ],
            [
                'cat_id'            => 'domain-w-runtimepath',
                'test_type'         => 'check',
                'with_runtime_path' => true,
            ],
            [
                'cat_id'            => 'redirect-full-wildcard',
                'test_type'         => 'redirect',
                'with_runtime_path' => false,
            ],
            [
                'cat_id'            => 'redirect-full-match',
                'test_type'         => 'redirect',
                'with_runtime_path' => false,
            ],
        ];
        foreach($test_list as $test) {

            if(isset($test['cat_id'])) {
                $test_values = $this->cache->json(__DIR__ . '/config/test-value-' . $test['cat_id'] . '.json', true);

                // todo: add logic to read according jsons (which need to be created)
                if('redirect-full-wildcard' === $test['cat_id'] ||
                    'redirect-full-match' === $test['cat_id']) {
                    $redirect_list = [
                        "full" => [
                            "*"              => [
                                "fill-data/foo/bar"              => [
                                    "to" => [
                                        "path" => "target/foo/bar",
                                    ],
                                ],
                                "fill-data-2/foo/bar"            => [
                                    "to" => [
                                        "path" => "target/foo/bar",
                                    ],
                                ],
                                "redirect-full-wildcard/foo/bar" => [
                                    "to" => [
                                        "path" => "target-passed/foo/bar",
                                    ],
                                ],
                                "fill-data-3/foo/bar"            => [
                                    "to" => [
                                        "path" => "target/foo/bar",
                                    ],
                                ],
                            ],
                            "tools.bemit.eu" => [
                                "fill-data/foo/bar"           => [
                                    "to" => [
                                        "path" => "target/foo/bar",
                                    ],
                                ],
                                "fill-data-2/foo/bar"         => [
                                    "to" => [
                                        "path" => "target/foo/bar",
                                    ],
                                ],
                                "redirect-full-match/foo/bar" => [
                                    "to" => [
                                        "path" => "target-passed/foo/bar",
                                    ],
                                ],
                                "fill-data-3/foo/bar"         => [
                                    "to" => [
                                        "path" => "target/foo/bar",
                                    ],
                                ],
                            ],
                        ],
                    ];
                    $tmp_val = [];
                    foreach($test_values as $id => $value) {
                        $tmp_val[$id] = $value;
                        $tmp_val[$id]['redirect_list'] = $redirect_list;
                    }
                    $test_values = $tmp_val;
                    unset($tmp_val);
                    /*
                    "full": {
                        "*": {
                            "redirect/foo/bar": {
                                "to": {
                                    "path": "target/foo/bar"
                                }
                            }
                        }
                    },
                    */
                }

                foreach($test_values as $id => $value) {
                    if($test['with_runtime_path']) {
                        // test with runtime_path
                        //$run_id = $this->emulator->run(array_merge(['runtime_path' => 'miniroute'], $value));
                        $emulator_result = $this->emulator->run(array_merge(['runtime_path' => 'miniroute'], $value));
                    } else {
                        // test without runtime_path
                        //$run_id = $this->emulator->run($value);
                        $emulator_result = $this->emulator->run($value);
                    }
                    // suffix the id with the cat id, so it can be displayed in the card header
                    $id = $id . ' - ' . $test['cat_id'];

                    if(false !== $emulator_result) {
                        $this->test_result[$test['cat_id']][$id] = new ResultTest();
                        /**
                         * @var \Bemit\MiniRouteTest\ResultTest $tmp_result
                         */
                        $tmp_result = &$this->test_result[$test['cat_id']][$id];
                        // when emulation was successfull, set the results
                        if(null !== $emulator_result->getResultCheck()) {
                            $tmp_result->setResultCheck($emulator_result->getResultCheck());
                        }
                        if(null !== $emulator_result->getResultRedirect()) {
                            $tmp_result->setResultRedirect($emulator_result->getResultRedirect());
                        }
                        if(null !== $emulator_result->getRuntimePath()) {
                            $tmp_result->setRuntimePath($emulator_result->getRuntimePath());
                        }

                        $tmp_result->comment_schema = $value['comment-schema'];
                        $tmp_result->comment = $value['comment'];

                        $tmp_result->setResult($value['result']);
                        $tmp_result->setTestType($test['test_type']);
                        $tmp_result->setActive($emulator_result->getActive());

                        if('check' === $test['test_type'] && null !== $emulator_result->getResultCheck()->getNew()) {
                            // when testing check, check for the emulator results about checking
                            if($emulator_result->getResultCheck()->getNew()->getBuildedUri() === $value['result']) {
                                $tmp_result->setPassed(true);
                            } else {
                                $tmp_result->setPassed(false);
                            }
                        }

                        if('redirect' === $test['test_type']) {
                            if(false !== $emulator_result->getResultRedirect()) {
                                if($emulator_result->getResultRedirect()->getDo() === $value['result']) {
                                    // bool: false only, result should be that getDo is false, so no redirection is done
                                    $tmp_result->setPassed(true);
                                } else if($emulator_result->getResultRedirect()->getToUriSchema() !== null) {
                                    // only when result redirect object is set
                                    if($emulator_result->getResultRedirect()->getToUriSchema()->getBuildedUri() === $value['result']) {
                                        // string: url, result should be the url that will be builded when a redirection should be executed
                                        $tmp_result->setPassed(true);
                                    } else {
                                        $tmp_result->setPassed(false);
                                    }
                                } else {
                                    $tmp_result->setPassed(false);
                                }
                            } else {
                                $tmp_result->setPassed(false);
                            }
                        }
                    }
                }
            }
            $test_values = null;
        }
    }
}