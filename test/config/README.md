# Automatic Build and Check Test

## Config and Input Possibilities

### [URI Build Config](uri-build.json) 

`ssl:www:trailing_slash:code:subdomain`

The first numbers list ssl, www, trailing_slash and subdomain. 0 is false, 1 is true.

They also count from 0 to 15 in binary numeral system, which is used in getting the config for the test case.

with 16 different combinations:

0. `0000 - 0:0:0:301:0`
1. `0001 - 0:0:0:301:1`
2. `0010 - 0:0:1:301:0`
3. `0011 - 0:0:1:301:1`
4. `0100 - 0:1:0:301:0`
5. `0101 - 0:1:0:301:1`
6. `0110 - 0:1:1:301:0`
7. `0111 - 0:1:1:301:1`
8. `1000 - 1:0:0:301:0`
9. `1001 - 1:0:0:301:1`
10. `1010 - 1:0:1:301:0`
11. `1011 - 1:0:1:301:1`
12. `1100 - 1:1:0:301:0`
13. `1101 - 1:1:0:301:1`
14. `1110 - 1:1:1:301:0`
15. `1111 - 1:1:1:301:1`

### Test Values

Check

- [Domain](test-value-domain.json)
- [Domain with runtime path](test-value-domain-w-runtimepath.json)
- [Domain + Path](test-value-path.json)
- [Domain + Path with runtime path](test-value-path-w-runtimepath.json)

Redirect

- [Full](test-value-redirect-full-wildcard.json)

`ssl:www:trailing_slash`

The input test values have the different parts that could be diffent:

- SSL is of or on
- hostname has www or not
- path has trailing slash or not

with 8 different combinations:

0. `000 - 0:0:0`
1. `001 - 0:0:1`
2. `010 - 0:1:0`
3. `011 - 0:1:1`
4. `100 - 1:0:0`
5. `101 - 1:0:1`
6. `110 - 1:1:0`
7. `111 - 1:1:1`


### Redirect Specials

At redirects the result can not only be a `string` but `false` too. A `string` must be an absolute [URI](https://tools.ietf.org/html/rfc3986#page-27) (e.g. http://example.com/foo/bar), when false no redirection was found so it `result` matches `getDo`.