<?php

namespace Bemit\MiniRouteTest;

/**
 * Container for the result of MiniRouteTest
 *
 * @category
 * @package    \Bemit\MiniRoute
 * @author     Original Author mb@project.bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0.2.0
 * @version    0.1.0
 */

class ResultTest {

    /**
     * @var \Bemit\MiniRoute\ResultChecker|null
     */
    protected $result_check;

    /**
     * @var \Bemit\MiniRoute\ResultRedirector|null
     */
    protected $result_redirect;

    /**
     * @var string
     */
    protected $result;

    /**
     * @var string
     */
    protected $runtime_path;

    /**
     * @var string
     */
    protected $test_type;

    /**
     * @var bool
     */
    protected $passed;

    /**
     * @var string
     */
    public $comment_schema;

    /**
     * @var array
     */
    public $comment;

    /**
     * @var \Bemit\MiniRoute\UriSchema|null
     */
    protected $active;

    public function __construct() {
        $this->result_check = null;
        $this->result_redirect = null;
        $this->result = '';

        $this->runtime_path = '';

        $this->test_type = '';

        $this->passed = false;

        $this->comment_schema = '';
        $this->comment = [];

        $this->active = null;
    }

    /**
     * @param \Bemit\MiniRoute\UriSchema $active
     */
    public function setActive($active) {
        $this->active = $active;
    }

    /**
     * @return \Bemit\MiniRoute\UriSchema
     */
    public function getActive() {
        return $this->active;
    }

    /**
     * @param \Bemit\MiniRoute\ResultChecker $result_check
     */
    public function setResultCheck($result_check) {
        $this->result_check = $result_check;
    }

    /**
     * @return \Bemit\MiniRoute\ResultChecker|null
     */
    public function getResultCheck() {
        return $this->result_check;
    }

    /**
     * @param $result_redirect
     */
    public function setResultRedirect($result_redirect) {
        $this->result_redirect = $result_redirect;
    }

    /**
     * @return \Bemit\MiniRoute\ResultRedirector|null
     */
    public function getResultRedirect() {
        return $this->result_redirect;
    }

    /**
     * @param $result
     */
    public function setResult($result) {
        $this->result = $result;
    }

    /**
     * @return string
     */
    public function getResult() {
        return $this->result;
    }

    /**
     * @param $runtime_path
     */
    public function setRuntimePath($runtime_path) {
        $this->runtime_path = $runtime_path;
    }

    /**
     * @return string
     */
    public function getRuntimePath() {
        return $this->runtime_path;
    }

    /**
     * @param $test_type
     */
    public function setTestType($test_type) {
        $this->test_type = $test_type;
    }

    /**
     * @return string
     */
    public function getTestType() {
        return $this->test_type;
    }

    /**
     * @param $passed
     */
    public function setPassed($passed) {
        $this->passed = $passed;
    }

    /**
     * @return bool
     */
    public function getPassed() {
        return $this->passed;
    }
}