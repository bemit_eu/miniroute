<?php

require __DIR__ . '/../../view/layout/head.php';
require __DIR__ . '/../../view/layout/logo.php';
require __DIR__ . '/../../view/layout/footer.php';
require_once '_result-check.php';
require_once '_result-redirect.php';

use Bemit\MiniRouteTest\MiniRouteTest;

if(!isset($miniroute_test)) {
    // for ide autocomplete in this file
    $miniroute_test = new MiniRouteTest();
}

?>
<!DOCTYPE html>
<html>
<head>
    <?php defaultHead(['title' => 'MiniRoute Test Result']); ?>
</head>
<body>
<div class="container-outer miniroute-test">
    <?php defaultLogo(); ?>
    <div class="container content">
        <h1 id="top">MiniRoute Test Result</h1>
        <div class="col-24 tested-category">
            <?php
            $qty = 0;
            foreach($miniroute_test->test_result as $category_id => $result_list) {
                $qty = count($result_list) + $qty;
                ?>
                <a href="#cat-id-<?= $category_id ?>" data-toggle="#cat-id-<?= $category_id ?>"><?= $category_id ?></a>
                <?php
            }
            ?>
            <a href="#combination-information">combination-information</a>
        </div>
        <div class="col-24">
            <p>There where <b><?= $qty ?></b> combinations of input hostname/path and config values tested.</p>
        </div>
        <?php
        $result_analyst = [
            'passed'          => 0,
            'failed'          => 0,
            'complete-passed' => true,
            'check'           => [
                'passed'  => 0,
                'failed'  => 0,
                'general' => true,
            ],
            'redirect'        => [
                'passed'  => 0,
                'failed'  => 0,
                'general' => true,
            ],
        ];
        foreach($miniroute_test->test_result as $category_id => $result_list) {
            ?>
            <div id="cat-id-<?= $category_id ?>" class="category-headline" data-click="on" data-toggle="#toggle-<?= $category_id ?>-content"><?= $category_id ?> -
                <b><?= count($result_list) ?></b></div>
            <div id="toggle-<?= $category_id ?>-content" class="category-content">
                <?php
                foreach($result_list as $id => $result) {
                    /**
                     * @var \Bemit\MiniRouteTest\ResultTest $result
                     */
                    if($result->getPassed()) {
                        $result_analyst['passed']++;
                        if('check' === $result->getTestType()) {
                            $result_analyst['check']['passed']++;
                        } else if('redirect' === $result->getTestType()) {
                            $result_analyst['redirect']['passed']++;
                        }
                    } else {
                        $result_analyst['failed']++;
                        $result_analyst['complete-passed'] = false;

                        if('check' === $result->getTestType()) {
                            $result_analyst['check']['failed']++;
                            $result_analyst['check']['general'] = false;
                        } else if('redirect' === $result->getTestType()) {
                            $result_analyst['redirect']['failed']++;
                            $result_analyst['redirect']['general'] = false;
                            $result_analyst['redirect']['general'] = false;
                        }
                    }
                    ?>
                    <fieldset class="test-result-<?= ($result->getPassed() ? 'passed' : 'failed') ?> hidden-content hidden">
                        <legend data-click="on" data-toggle="#toggle-<?= str_replace(' ', '', $id) ?>-content"><?= $id . ': ' . ($result->getPassed() ? 'passed' : 'failed') ?></legend>
                        <div id="toggle-<?= str_replace(' ', '', $id) ?>-content" class="hidden">
                            <?php
                            if('check' === $result->getTestType()) {
                                _resultCheck($result, ['headline' => 'Check Test Result']);
                            } else if('redirect' === $result->getTestType()) {
                                _resultRedirect($result, ['headline' => 'Redirect Test Result']);
                            }
                            ?>
                        </div>
                    </fieldset>
                    <?php
                }
                ?>
            </div>
            <?php
        }
        ?>
        <div class="col-24 build-status inline">
            <?php include '_build-status.php' ?>
        </div>
        <div class="col-24 combination-information" id="combination-information">
            <h3 class="h3">Information About Combinations Checked</h3>
            <p>There are different input values checked:</p>
            <ul>
                <li>path - input value set with hostname (e.g. example.org) and a path (e.g. foo/bar)</li>
                <li>path w runtimepath - like previous, but now emulating with 'MiniRoute is installed in subdirectory' where subdirectory = runtime_path</li>
                <li>domain - input value set with hostname (e.g. example.org), but path is empty</li>
                <li>domain w runtimepath - like previous, but now emulating with 'MiniRoute is installed in subdirectory' where subdirectory = runtime_path</li>
            </ul>
            <p>Each type of input value, is checked with all combinations of input and checked against all combinations of config.</p>
            <table class="info-combi-config">
                <tr>
                    <th colspan="5">Configuration Combinations - <b>16</b></th>
                </tr>
                <tr>
                    <td></td>
                    <td>SSL</td>
                    <td>WWW</td>
                    <td>Trailing Slash</td>
                    <td>Subdomain</td>
                </tr>
                <?php
                for($i = 0; $i < 16; $i++) {
                    $combination_config = $miniroute_test->binary_trueness->generate($i, 4);
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td class="<?= (substr($combination_config, 0, 1) ? 'on' : 'off') ?>"><?= (substr($combination_config, 0, 1) ? 'on' : 'off') ?></td>
                        <td class="<?= (substr($combination_config, 1, 1) ? 'on' : 'off') ?>"><?= (substr($combination_config, 1, 1) ? 'on' : 'off') ?></td>
                        <td class="<?= (substr($combination_config, 2, 1) ? 'on' : 'off') ?>"><?= (substr($combination_config, 2, 1) ? 'on' : 'off') ?></td>
                        <td class="<?= (substr($combination_config, 3, 1) ? 'on' : 'off') ?>"><?= (substr($combination_config, 3, 1) ? 'on' : 'off') ?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <table class="info-combi-input">
                <tr>
                    <th colspan="4">Input Combinations - <b>8</b></th>
                </tr>
                <tr>
                    <td></td>
                    <td>SSL</td>
                    <td>WWW</td>
                    <td>Trailing Slash</td>
                </tr>
                <?php
                for($i = 0; $i < 8; $i++) {
                    $combination_input = $miniroute_test->binary_trueness->generate($i, 3);
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td class="<?= (substr($combination_input, 0, 1) ? 'on' : 'off') ?>"><?= (substr($combination_input, 0, 1) ? 'on' : 'off') ?></td>
                        <td class="<?= (substr($combination_input, 1, 1) ? 'on' : 'off') ?>"><?= (substr($combination_input, 1, 1) ? 'on' : 'off') ?></td>
                        <td class="<?= (substr($combination_input, 2, 1) ? 'on' : 'off') ?>"><?= (substr($combination_input, 2, 1) ? 'on' : 'off') ?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <p>At redirections there are tested matches which use a hostname wildcard as mapping, an those that doesn't. Also is each match type tested with each type of combination and also that a URI that is not mapped will not be redirected. Making 8 Input URI Combinations, 16 Config Combinations + no-redirection = 136 tests per match types</p>
            <p>Match Types</p>
            <table>
                <tr>
                    <th>hostname</th>
                    <th>path</th>
                </tr>
                <tr>
                    <td>wildcard</td>
                    <td>match</td>
                </tr>
                <tr>
                    <td>match</td>
                    <td>match</td>
                </tr>
            </table>
        </div>
    </div>

    <div class="footer-wrapper">
        <div class="container">
            <?php defaultFooter(); ?>
        </div>
    </div>

    <a href="#top" class="to-top">^</a>
</div>
</body>
</html>
