<div>
    <div>
        <span class="label">General</span><span class="general-status <?= ($result_analyst['complete-passed'] ? 'passed' : 'failed') ?>"><?= ($result_analyst['complete-passed'] ? 'passed' : 'failed') ?></span>
    </div>
    <div><span class="have-passed"><?= $result_analyst['passed'] ?></span></div>
    <div><span class="have-failed"><?= $result_analyst['failed'] ?></span></div>
</div>

<div>
    <div>
        <span class="label">Check</span><span class="general-status <?= ($result_analyst['check']['general'] ? 'passed' : 'failed') ?>"><?= ($result_analyst['check']['general'] ? 'passed' : 'failed') ?></span>
    </div>
    <div><span class="have-passed"><?= $result_analyst['check']['passed'] ?></span></div>
    <div><span class="have-failed"><?= $result_analyst['check']['failed'] ?></span></div>

</div>

<div>
    <div>
        <span class="label">Redirect</span><span class="general-status <?= ($result_analyst['redirect']['general'] ? 'passed' : 'failed') ?>"><?= ($result_analyst['redirect']['general'] ? 'passed' : 'failed') ?></span>
    </div>
    <div><span class="have-passed"><?= $result_analyst['redirect']['passed'] ?></span></div>
    <div><span class="have-failed"><?= $result_analyst['redirect']['failed'] ?></span></div>
</div>