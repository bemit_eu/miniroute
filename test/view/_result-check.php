<?php

/**
 * @param \Bemit\MiniRouteTest\ResultTest $result
 */
function _resultCheck($result, $data = []) {
    if('' !== $result->getTestType()) {
        //$result_type = $result->getTestType() . '_result';

        $result_new = false;
        $result_old = false;

        if('check' == $result->getTestType()) {
            /**
             * @var \Bemit\MiniRoute\UriSchema $result_old
             */
            $result_old = $result->getResultCheck()->getOld();
            /**
             * @var \Bemit\MiniRoute\UriSchema $result_new
             */
            $result_new = $result->getResultCheck()->getNew();
        }

        if('redirect' == $result->getTestType() && false !== $result->getResultRedirect()) {
            /**
             * @var \Bemit\MiniRoute\UriSchema $result_old
             */
            $result_old = $result->getResultRedirect()->getOld();
            /**
             * @var \Bemit\MiniRoute\UriSchema $result_new
             */
            $result_new = $result->getResultRedirect()->getToUriSchema();
        }

        if(false !== $result_new && false !== $result_old) {
            ?>
            <table>
                <tr>
                    <th colspan="2" class="h3"><?= $data['headline'] ?></th>
                </tr>
                <tr>
                    <th>Item</th>
                    <th>Old</th>
                    <th>New</th>
                </tr>
                <tr>
                    <td>Hostname</td>
                    <td><?= $result_old->getUri()['hostname'] ?></td>
                    <td><?= $result_new->getUri()['hostname'] ?></td>
                </tr>
                <tr>
                    <td>Path</td>
                    <td><?= $result_old->getUri()['path'] ?></td>
                    <td><?= $result_new->getUri()['path'] ?></td>
                </tr>
                <tr>
                    <td>Param</td>
                    <td>(TODO)<?php
                        if(isset($result_old->getUri()['param'])) {
                            $result_old->getUri()['param'];
                        }
                        ?></td>
                    <td>(TODO)<?php
                        if(isset($result_new->getUri()['param'])) {
                            $result_new->getUri()['param'];
                        }
                        ?></td>
                </tr>
                <tr>
                    <td>SSL</td>
                    <td><?= ($result_old->getSsl() ? 'on' : 'off') ?></td>
                    <td
                        class="<?= (($result_new->getSsl() !== $result_old->getSsl()) ? 'alternate' : '') ?> <?= ($result_new->getSsl() ? 'on' : 'off') ?>"
                    ><?= ($result_new->getSsl() ? 'on' : 'off') ?></td>
                </tr>
                <tr>
                    <td>WWW</td>
                    <td><?= ($result_old->getWww() ? 'on' : 'off') ?></td>
                    <td
                        class="<?= ($result_new->getWww() !== $result_old->getWww() ? 'alternate' : '') ?> <?= ($result_new->getWww() ? 'on' : 'off') ?>"
                    ><?= ($result_new->getWww() ? 'on' : 'off') ?></td>
                </tr>
                <tr>
                    <td>Trailing Slash</td>
                    <td><?= ($result_old->getTrailingSlash() ? 'on' : 'off') ?></td>
                    <td
                        class="<?= ($result_new->getTrailingSlash() !== $result_old->getTrailingSlash() ? 'alternate' : '') ?> <?= ($result_new->getTrailingSlash() ? 'on' : 'off') ?>"
                    ><?= ($result_new->getTrailingSlash() ? 'on' : 'off') ?></td>
                </tr>
                <tr>
                    <td>Runtime Path</td>
                    <td><?= ('' !== $result->getRuntimePath() ? $result->getRuntimePath() : '-') ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Input URI</td>
                    <td colspan="2">
                        <code>http<?= ($result_old->getSsl() ? 's' : '') . '://' . ($result_old->getWww() ? 'www.' : '') . $result_old->getUri()['hostname'] . (!empty($result->getRuntimePath()) ? '/' . $result->getRuntimePath() : '') . (!empty($result_old->getUri()['path']) ? '/' . $result_old->getUri()['path'] : '') . ($result_old->getTrailingSlash() ? '/' : '') ?></code>
                    </td>
                </tr>
                <tr>
                    <td>Generated URI</td>
                    <td colspan="2"><code><?= ($result_new->getBuildedUri()) ?></code></td>
                </tr>
                <tr>
                    <td>Valid URI</td>
                    <td colspan="2"><code><?= ($result->getResult()) ?></code></td>
                </tr>
            </table>
            <p>This is the comment about how the from (input) and to (valid) differ in trueness table.</p>
            <p><code>ssl:www:trailing_slash</code> - Schema, from</p>
            <p><code>ssl:www:trailing_slash:subdomain</code> - Schema, to</p>
            <table>
                <tr>
                    <td>From</td>
                    <td></td>
                    <td>To</td>
                </tr>
                <tr>
                    <td><?= ($result->comment['from']) ?></td>
                    <td>-></td>
                    <td><?= ($result->comment['to']) ?></td>
                </tr>
            </table>
            <?php
        } else {
            echo 'Exception:_result:result new || old not set';
        }
    } else {
        echo 'Exception:_result:test-type not set';
    }
}