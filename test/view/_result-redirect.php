<?php

/**
 * @param \Bemit\MiniRouteTest\ResultTest $result
 */
function _resultRedirect($result, $data = []) {
    if('' !== $result->getTestType()) {
        //$result_type = $result->getTestType() . '_result';

        if(null !== $result->getResultRedirect()) {
            $show_info_small = false;
            if($result->getPassed()) {
                if($result->getResultRedirect()->getDo()) {
                    ?>
                    <p>A redirection is done!</p>
                    <table>
                        <tr>
                            <th colspan="2" class="h3"><?= $data['headline'] ?></th>
                        </tr>
                        <tr>
                            <th>Item</th>
                            <th>Old</th>
                            <th>New</th>
                        </tr>
                        <tr>
                            <td>Hostname</td>
                            <td><?= $result->getActive()->getUri()['hostname'] ?></td>
                            <td><?= $result->getResultRedirect()->getToUriSchema()->getUri()['hostname'] ?></td>
                        </tr>
                        <tr>
                            <td>Path</td>
                            <td><?= $result->getActive()->getUri()['path'] ?></td>
                            <td><?= $result->getResultRedirect()->getToUriSchema()->getUri()['path'] ?></td>
                        </tr>
                        <tr>
                            <td>Param</td>
                            <td>(TODO)<?php
                                if(isset($result->getActive()->getUri()['param'])) {
                                    $result->getActive()->getUri()['param'];
                                }
                                ?></td>
                            <td>(TODO)<?php
                                if(isset($result->getResultRedirect()->getToUriSchema()->getUri()['param'])) {
                                    $result->getResultRedirect()->getToUriSchema()->getUri()['param'];
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <td>SSL</td>
                            <td><?= ($result->getActive()->getSsl() ? 'on' : 'off') ?></td>
                            <td><?= ($result->getResultRedirect()->getToUriSchema()->getSsl() ? 'on' : 'off') ?></td>
                        </tr>
                        <tr>
                            <td>WWW</td>
                            <td><?= ($result->getActive()->getWww() ? 'on' : 'off') ?></td>
                            <td><?= ($result->getResultRedirect()->getToUriSchema()->getWww() ? 'on' : 'off') ?></td>
                        </tr>
                        <tr>
                            <td>Trailing Slash</td>
                            <td><?= ($result->getActive()->getTrailingSlash() ? 'on' : 'off') ?></td>
                            <td><?= ($result->getResultRedirect()->getToUriSchema()->getTrailingSlash() ? 'on' : 'off') ?></td>
                        </tr>
                        <tr>
                            <td>Runtime Path</td>
                            <td colspan="2"><?= ('' !== $result->getRuntimePath() ? $result->getRuntimePath() : '-') ?></td>
                        </tr>
                        <tr>
                            <td>Input URI</td>
                            <td colspan="2">
                                only hostname and<br>path are evaluated<br>from result redirect
                            </td>
                        </tr>
                        <tr>
                            <td>Generated Target</td>
                            <td colspan="2">
                                <code><?= ($result->getResultRedirect()->getToUriSchema()->getBuildedUri()) ?></code>
                            </td>
                        </tr>
                        <tr>
                            <td>Generated Target</td>
                            <td colspan="2">
                                <code><?= ($result->getResultRedirect()->getToUriSchema()->getBuildedUri()) ?></code>
                            </td>
                        </tr>
                    </table>
                    <p>This is the comment about how the from (input) and to (valid) differ in trueness table.</p>
                    <p><code>ssl:www:trailing_slash</code> - Schema, from</p>
                    <p><code>ssl:www:trailing_slash:subdomain</code> - Schema, to</p>
                    <table>
                        <tr>
                            <td>From</td>
                            <td></td>
                            <td>To</td>
                        </tr>
                        <tr>
                            <td><?= ($result->comment['from']) ?></td>
                            <td>-></td>
                            <td><?= ($result->comment['to']) ?></td>
                        </tr>
                    </table>
                    <?php
                } else {
                    echo '<p>No redirection is done!</p>';
                    $sub_line = 'Valid Result is:';
                    $show_info_small = true;
                }
            } else {
                echo '<p>Failed!</p>';
                $sub_line = 'Result should have been:';
                $show_info_small = true;
            }

            if($show_info_small) {
                ?>
                <p><?= $sub_line ?>
                    <code><?= (is_bool($result->getResult()) ? var_export($result->getResult()) : $result->getResult()) ?></code>
                </p>
                <p><code>Redirector->Do</code> is:
                    <code><?php var_export($result->getResultRedirect()->getDo()); ?></code></p>
                <p>Triaged target:
                    <?php
                    if($result->getResultRedirect()->getDo()) {
                        echo '<code>' . $result->getResultRedirect()->getToUriSchema()->getBuildedUri() . '</code>';
                    } else {
                        echo '<code>-</code>';
                    }
                    ?>
                </p>
                <p>Runtime path
                    <code><?= ((0 < strlen($result->getRuntimePath())) ? $result->getRuntimePath() : '-') ?></code>
                </p>
                <div><p>The input with hostname
                        <code><?= $result->getActive()->getUri()['hostname'] ?></code> and path
                        <code><?= $result->getActive()->getUri()['path'] ?></code> will not be redirected.</p>
                </div>
                <?php
            }
        } else {
            echo 'Exception:_result:getResultRedirect not set';
        }
    } else {
        echo 'Exception:_result:test-type not set';
    }
}