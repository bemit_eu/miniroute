<?php

namespace Bemit\MiniRouteTest;

/**
 * For config truenesstable generation
 *
 * @category
 * @package    \Bemit\MiniRouteTest
 * @author     Original Author mb@project.bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0.1.0
 * @version    0.0.1
 */
class BinaryTrueness {

    /**
     * @param int $number
     * @param int $len how long the format should be, eg. $number = 2, return 10 but with $len = 4, return 0010
     *
     * @return string the given $number in binary format with filling zeros
     */
    public function generate($number, $len = 4) {
        return sprintf('%0' . $len . 'b', $number);
    }
}