<?php

namespace Bemit\MiniRouteTest;

use Bemit\MiniRoute\Cache;
use Bemit\MiniRoute\MiniRoute;

/**
 * Saves the information about a specific uri schema
 *
 * Uri schemas define how the uri should be build and cleaned and things like that
 *
 * @category
 * @package    \Bemit\MiniRouteTest
 * @author     Original Author mb@project.bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0.1.0
 * @version    0.1.0
 */
class Emulator {

    public $result = [];

    protected $test_i = null;

    protected $binary_trueness;

    protected $cache;

    public function __construct() {
        $this->cache = new Cache();
        $this->cache->init(['on' => false]);
        $this->binary_trueness = new BinaryTrueness();
    }

    /**
     * @param $test_value
     *
     * @return \Bemit\MiniRouteTest\ResultEmulator|bool
     */
    public function run($test_value) {
        // var_dump($test_value);
        // Runtime Container of MiniRoute, in this the actual classes are constructed and handled
        $mini_route = new MiniRoute();

        $mini_route->initCache(['on' => false]);

        if(isset($test_value['runtime_path'])) {
            $mini_route->addRuntimePath($test_value['runtime_path']);
        }

        // Must be called before route->route()
        $mini_route->addUriActive([
            'hostname'   => $test_value['from']['hostname'],
            'path-query' => $test_value['from']['path-query'],
        ]);

        if(isset($test_value['from']['ssl'])) {
            $mini_route->getUri()->setActiveSsl($test_value['from']['ssl']);
        }

        //$mini_route->uriAddCollection('config/uri.json');
        $uri_build = $this->cache->json(__DIR__ . '/config/uri-build.json', true);

        $general_config_id = 'general-' . $this->binary_trueness->generate($test_value['config']);
        if(isset($uri_build[$general_config_id])) {
            $mini_route->addUriBuildConfig([
                'general' => $uri_build[$general_config_id],
            ]);

            if(isset($test_value['redirect_list'])) {
                $mini_route->addRedirect($test_value['redirect_list']);
            }

            $mini_route->route(true);

            $result = new ResultEmulator();
            $result->setResultRedirect($mini_route->getRedirectResult());
            $result->setResultCheck($mini_route->getCheckResult());
            $result->setRuntimePath($mini_route->getUri()->getRuntimePath());
            $result->setActive($mini_route->active_schema);

            //$run_id = count($this->result) - 1;
        } else {
            echo 'Test General Config not found, aborting.';
            $result = false;
        }

        return $result;
    }
}