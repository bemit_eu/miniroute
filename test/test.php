<?php
/**
 * Hook File for executing test and integrating result view
 *
 * Only can be used with htaccess renamed to .htaccess and test miniroute mod_rewrite rule.
 *
 * Intended for the use of contributes of MiniRoute not production testing!
 */

error_reporting(E_ALL);

if(isset($_GET['miniroute-debug'])) {
    $option['debug'] = (filter_input(INPUT_GET, 'miniroute-debug', FILTER_SANITIZE_NUMBER_INT) ? true : false);
} else {
    $option['debug'] = true;
}

require_once __DIR__ . '/autoload.php';
require_once __DIR__ . '/../autoload.php';
require_once __DIR__ . '/../vendor/autoload.php';

use \Bemit\MiniRouteTest\MiniRouteTest;

$miniroute_test = new MiniRouteTest();
$miniroute_test->run();

require __DIR__ . '/view/view-test.php';