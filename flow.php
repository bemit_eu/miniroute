<?php

//
// Just for switching into test, test is intended for contributors of MiniRoute and not production test as it depends on some exact placements
//
// SEE: display.php
//

// just for test switch, remove clause for production!
if(1 === strpos(filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL), 'miniroute/miniroute.test')) {
    require 'test/test.php';
} else {
    require 'display.php';
}