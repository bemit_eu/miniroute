<?php

/**
 * An example implementation of MiniRoute
 *
 * Please copy and modify according to your needed implementation/integration!
 *
 * The file 'htaccess' need to be renamed to '.htaccess' and also must be modified according to your needs.
 */

$option['run']['time']['begin'] = microtime(true); // Set time value for performance monitor
$option['run']['memory']['begin'] = memory_get_usage(); // Set memory value for performance monitor


if(isset($_GET['miniroute-debug'])) {
    $option['debug'] = (filter_input(INPUT_GET, 'miniroute-debug', FILTER_SANITIZE_NUMBER_INT) ? true : false);
} else {
    $option['debug'] = true;
}

require_once 'autoload.php';
require_once 'vendor/autoload.php';

// Usage of monitor is optinal
use Bemit\MiniRoute\MiniRoute;
use Flood\Component\PerformanceMonitor\Monitor;

Monitor::i()->startProfile('miniroute-complete', [
    'time'   => $option['run']['time']['begin'],
    'memory' => $option['run']['memory']['begin'],
]);

error_reporting(E_ALL);

Monitor::i()->startProfile('miniroute-logic-complete');

// Runtime Container of MiniRoute, in this the actual classes are constructed and handled
$mini_route = new MiniRoute();

Monitor::i()->startProfile('miniroute-logic-init-complete');

$mini_route->initCache([
    'path'       => 'tmp/miniroute',
    'on'         => true,
    'check_time' => true,
]);

// Must, when needed, be called before Uri->addActive
$mini_route->addRuntimePath('miniroute');

// Must be called before route->route()
$mini_route->addUriActive();

$mini_route->addUriCollection('config/uri.json');
$mini_route->addUriBuildConfig('config/uri-build.json');
$mini_route->addRedirect('config/redirect.json');

Monitor::i()->endProfile('miniroute-logic-init-complete');
Monitor::i()->startProfile('miniroute-logic-route');

$mini_route->route($option['debug']);

Monitor::i()->endProfile('miniroute-logic-route');
Monitor::i()->endProfile('miniroute-logic-complete');

Monitor::i()->endProfile('miniroute-complete');

// Here would be your CMS, App, Shop or what ever be included
require 'view/default.php';