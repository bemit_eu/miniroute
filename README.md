# README #

MiniRoute, a PHP URI redirector/cleaner

`composer require bemit/miniroute` - when using as module in existing app

`composer create-project bemit/miniroute` - when using as redirector

# Usage
MiniRoute uses a routing array/object to check if the active URI should be redirected in some way. Various scenarios are available for redirection and cleaning.  

# Licence
This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more informations on the Licences which are applied read: [LICENCE.md](LICENCE.md)

# Version
Version 0.2.0

# Made by
Michael Becker, mb@project.bemit.eu