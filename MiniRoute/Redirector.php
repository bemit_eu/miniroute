<?php

namespace Bemit\MiniRoute;

/**
 * Redirects the called URI when needed
 *
 * @category
 * @package    \Bemit\MiniRoute
 * @author     Original Author mb@project.bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0.1.0
 * @version    0.1.1
 */

class Redirector {

    /**
     * contains the redirect targets, to which an uri should be redirected when the from matches something in the list
     *
     * @var array
     */
    protected $redirect_list = [];

    /**
     * @var \Bemit\MiniRoute\MiniRoute
     */
    protected $mini_route;

    /**
     * @param \Bemit\MiniRoute\MiniRoute $mini_route
     */
    public function __construct(&$mini_route) {
        $this->mini_route = $mini_route;
    }

    /**
     * @param string|array $redirect Path to json of redirect or redirect as array
     *
     * @return bool|null null when nothing was okay, true when successfully added, false when not
     */
    public function addRedirect($redirect) {
        if(is_string($redirect)) {
            return $this->addRedirectString($redirect);
        } else if(is_array($redirect)) {
            return $this->addRedirectArray($redirect);
        } else {
            return null;
        }
    }

    /**
     * @param string $redirect Path to redirect file
     *
     * @return bool
     */
    protected function addRedirectString($redirect) {
        $return_val = false;
        if(is_string($redirect) && is_file($redirect)) {
            $this->redirect_list = array_merge($this->redirect_list, $this->mini_route->getCache()->json($redirect, true));
            $return_val = true;
        }

        return $return_val;
    }

    /**
     * @param array $redirect
     *
     * @return bool
     */
    protected function addRedirectArray($redirect) {
        $return_val = false;
        if(is_array($redirect)) {
            $this->redirect_list = array_merge($this->redirect_list, $redirect);
            $return_val = true;
        }

        return $return_val;
    }

    public function getRedirectList() {
        return $this->redirect_list;
    }

    /**
     * @param bool $debug
     *
     * @return \Bemit\MiniRoute\ResultRedirector
     */
    public function redirect($debug = false) {
        $match_list = [];
        // what types of matches exist
        $match_list_type = [
            // domainpart:pathpart
            'match:match',
            'wildcard:match',
        ];

        $result = new ResultRedirector();
        $result->setDo(false);

        // check the redirection list for existing mappings
        // when there is a list type 'full'
        if(isset($this->getRedirectList()['full'])) {
            foreach($this->getRedirectList()['full'] as $hostname => $redirect_schema_element) {
                // go through each routing mapping, eaching mapping is represented by a uri schema
                if('*' === $hostname || $this->mini_route->getUri()->getActiveHostname(false) === $hostname) {
                    // when the hostname of active mapping is the same as the active hostname, the mapping belong to this hostname so search for redirections in it
                    if(array_key_exists($this->mini_route->getUri()->getActivePath(false), $redirect_schema_element)) {
                        // search if there is a redirection with the active path

                        /**
                         *
                         */
                        $tmp_data = $redirect_schema_element[$this->mini_route->getUri()->getActivePath(false)];//
                        if('*' === $hostname) {
                            $type = 'wildcard:match';
                            // when domain is a wildcard, the 'to hostname' must be set as active, so UriSchema will use the active to gather Config values
                            $tmp_data['to']['hostname'] = $this->mini_route->getUri()->getActiveHostname(false);
                        } else {
                            $type = 'match:match';
                        }
                        $tmp_data['uri'] = $tmp_data['to'];
                        unset($tmp_data['to']);
                        // make a uri schema out of the found, fill all not set field with config values
                        $match_list[$type][] = $this->mini_route->getUriSchema()->n(
                            $tmp_data,
                            true
                        );
                    }
                }
            }
            $build = false;
            /*
             * Go through each matched list to see if there should be performed a redirect
             */
            foreach($match_list_type as $type) {
                if(!empty($match_list[$type])) {
                    // first check if a full match happend at the full redirect
                    /**
                     * @var \Bemit\MiniRoute\UriSchema $build
                     */
                    $build = $this->mini_route->getUriBuild()->build($match_list[$type][0]);
                    break;
                }
            }
            if(false !== $build) {
                if(!$debug) {
                    $this->mini_route->getUri()->redirect($build->getBuildedUri(), $build->getCode());
                }
                // only need to be added as debug info, so could be after redirect
                $result->setDo(true);
                $result->setToUriSchema($build);
                $result->setCode($build->getCode());
            }
        }

        return $result;
    }
}