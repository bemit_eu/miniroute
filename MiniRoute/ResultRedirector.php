<?php

namespace Bemit\MiniRoute;

/**
 * Container for the result of Redirector
 *
 * @category
 * @package    \Bemit\MiniRoute
 * @author     Original Author mb@project.bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0.2.0
 * @version    0.1.1
 */

class ResultRedirector {

    /**
     * @var bool
     */
    protected $do;

    /**
     * The uri schema of the builded uri, the to part
     *
     * @var \Bemit\MiniRoute\UriSchema
     */
    protected $to_uri_schema;

    /**
     * @var int
     */
    protected $code;

    /**
     * @var \Bemit\MiniRoute\UriSchema|null
     */
    protected $uri;

    public function __construct() {
        $this->do = false;

        $this->to_uri_schema = null;

        $this->code = 0;

        $this->uri = null;
    }

    /**
     * @param $do
     */
    public function setDo($do) {
        $this->do = $do;
    }

    /**
     * @return bool
     */
    public function getDo() {
        return $this->do;
    }

    /**
     * @param $to_uri_schema
     */
    public function setToUriSchema($to_uri_schema) {
        $this->to_uri_schema = $to_uri_schema;
    }

    /**
     * Returns the URI schema of the target, to where the redirection should be done
     *
     * @return \Bemit\MiniRoute\UriSchema|null
     */
    public function getToUriSchema() {
        return $this->to_uri_schema;
    }

    /**
     * @param $code
     */
    public function setCode($code) {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getCode() {
        return $this->code;
    }
}