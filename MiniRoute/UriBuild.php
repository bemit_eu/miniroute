<?php

namespace Bemit\MiniRoute;

/**
 * Build the needed URI from the evaluated data given
 *
 * @category
 * @package    \Bemit\MiniRoute
 * @author     Original Author mb@project.bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0.0.2
 * @version    0.1.2
 */

class UriBuild {

    protected $build_config = [];

    /**
     * @var \Bemit\MiniRoute\MiniRoute
     */
    protected $mini_route;

    /**
     * @param \Bemit\MiniRoute\MiniRoute $mini_route
     */
    public function __construct(&$mini_route) {
        $this->mini_route = $mini_route;
    }

    /**
     * @param string|array $config Path to json of a uri config or the config as array
     *
     * @return bool|null null when nothing was okay, true when successfully added, false when not
     */
    public function addConfig($config) {
        if(is_string($config)) {
            return $this->addConfigString($config);
        } else if(is_array($config)) {
            return $this->addConfigArray($config);
        } else {
            return null;
        }
    }

    /**
     * @param string $config Path to routing file
     *
     * @return bool
     */
    protected function addConfigString($config) {
        $return_val = false;
        if(!is_file($config)) {
            echo "\r\n" . 'URI-Build Config could not be found: ' . $config . "\r\n";
        }
        if(is_string($config) && is_file($config)) {
            $this->build_config = array_merge($this->build_config, $this->mini_route->getCache()->json($config, true));
            $return_val = true;
        }

        return $return_val;
    }

    /**
     * @param array $config
     *
     * @return bool
     */
    protected function addConfigArray($config) {
        $return_val = false;
        if(is_array($config)) {
            $this->build_config = array_merge($this->build_config, $config);
            $return_val = true;
        }

        return $return_val;
    }

    /**
     * @param \Bemit\MiniRoute\UriSchema $target
     *
     * @return bool|\Bemit\MiniRoute\UriSchema
     */
    public function build($target) {
        $return = false;
        if($target->hasUri()) {

            $target->setBuildedUri($this->buildBySchema($target));

            $return = $target;
        }

        return $return;
    }

    /**
     * Builds the uri given from a specific schema
     *
     * @param \Bemit\MiniRoute\UriSchema $schema
     *
     * @return string
     */
    public function buildBySchema($schema) {
        $uri = '';
        // Protocol
        if($schema->getSsl()) {
            $uri .= 'https://';
        } else {
            $uri .= 'http://';
        }

        // some like the deprecated www. subdomain
        if($schema->getWww() && !$schema->getSubdomain()) {
            $uri .= 'www.';
        }

        // Hostname
        if($schema->hasUri('hostname')) {
            $hostname = $schema->getUri()['hostname'];
        } else {
            $hostname = $this->mini_route->getUri()->getActiveHostname(false);
        }
        $uri .= $hostname;

        // When needed: runtime_path
        if($this->mini_route->getUri()->getActiveHostname(false) === $hostname) {
            // when target and active hostname is same and runtime_path not empty, prepend it to path
            if(!empty($this->mini_route->getUri()->getRuntimePath())) {
                $uri .= '/' . $this->mini_route->getUri()->getRuntimePath();
            }
        }

        // Path
        if($schema->hasUri('path')) {
            $uri .= '/' . $this->mini_route->getUriHelper()->stripSlash($schema->getUri()['path']);
        }

        // Trailing Slash
        if($schema->getTrailingSlash()) {
            $uri .= '/';
        }

        return $uri;
    }

    /**
     * @param string|null $key
     * @param string      $scope
     *
     * @return mixed
     */
    public function getConfig($key = null, $scope = 'general') {
        if(null === $key) {
            if(isset($this->build_config[$scope])) {
                return $this->build_config[$scope];
            }
        } else {
            if(isset($this->build_config[$scope][$key])) {
                return $this->build_config[$scope][$key];
            }
        }
        // fallback to general config
        // when the config wanted was a scoped one, but not found, fetch the general as fallback
        if('general' !== $scope) {
            return $this->getConfig($key, 'general');
        }

        return null;
    }
}