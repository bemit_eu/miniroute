<?php

namespace Bemit\MiniRoute;

/**
 * MiniRoute Runtime: Contains and controls the runtime of MiniRoute
 *
 * This class is the starter for MiniRoute, it initiates all needed classes for you and provides the classes and those methods.
 *
 * @category
 * @package    \Bemit\MiniRoute
 * @author     Original Author mb@project.bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0.1.0
 * @version    0.1.0
 */

class MiniRoute implements iMiniRoute {

    /**
     * @var \Bemit\MiniRoute\Cache
     */
    protected $cache;

    /**
     * @var \Bemit\MiniRoute\Uri
     */
    protected $uri;

    /**
     * @var \Bemit\MiniRoute\UriBuild
     */
    protected $uri_build;

    /**
     * @var \Bemit\MiniRoute\UriHelper
     */
    protected $uri_helper;

    /**
     * @var \Bemit\MiniRoute\UriSchema
     */
    protected $uri_schema;

    /**
     * @var \Bemit\MiniRoute\Redirector
     */
    protected $redirector;

    /**
     * @var \Bemit\MiniRoute\Checker
     */
    protected $checker;

    /**
     * Contains the result after the uri was checked, for displaying information in default.php
     *
     * @var \Bemit\MiniRoute\ResultChecker
     */
    protected $check_result;

    /**
     * Contains the result after it was searched for a redirection, used here and as information in default.php
     *
     * @var \Bemit\MiniRoute\ResultRedirector
     */
    protected $redirect_result;


    /**
     * The schema of the input uri, this schema should not be changed, the value is first set
     *
     * @var \Bemit\MiniRoute\UriSchema|null
     */
    public $active_schema;

    public function __construct() {
        $this->cache = new Cache();

        $this->redirector = new Redirector($this);
        $this->checker = new Checker($this);

        $this->uri = new Uri($this);
        $this->uri_build = new UriBuild($this);
        $this->uri_helper = new UriHelper($this);
        $this->uri_schema = new UriSchema($this);
    }

    /**
     * @return \Bemit\MiniRoute\Cache
     */
    public function getCache() {
        return $this->cache;
    }

    /**
     * @return \Bemit\MiniRoute\Uri
     */
    public function getUri() {
        return $this->uri;
    }

    /**
     * @return \Bemit\MiniRoute\UriBuild
     */
    public function getUriBuild() {
        return $this->uri_build;
    }

    /**
     * @return \Bemit\MiniRoute\UriHelper
     */
    public function getUriHelper() {
        return $this->uri_helper;
    }

    /**
     * @return \Bemit\MiniRoute\UriSchema
     */
    public function getUriSchema() {
        return $this->uri_schema;
    }

    /**
     * @return \Bemit\MiniRoute\Redirector
     */
    public function getRedirector() {
        return $this->redirector;
    }

    /**
     * @param $option
     *
     * @return bool if cache was turned on or not
     */
    public function initCache($option) {
        return $this->cache->init($option);
    }

    /**
     * Adds the path as runtime path, meaning when using the software like example.org/host-dir host-dir must be given as runtime path. This will be
     * deleted from the beginning of all incoming URI paths.
     *
     * It will be prepended on all redirects where no hostname was added, but not on 'domain known' redirects AND on
     *
     * @param $path
     */
    public function addRuntimePath($path) {
        $this->uri->addRuntimePath($path);
    }

    /**
     * @param array $active when not set, it automatically sets the information
     */
    public function addUriActive($active = []) {
        $this->active_schema = $this->uri->addActive($active);
    }

    /**
     * @param string|array $collection Path to json of a uri collection or the collection as array
     *
     * @return bool|null null when nothing was okay, true when successfully added, false when not
     */
    public function addUriCollection($collection) {
        return $this->uri->addCollection($collection);
    }

    /**
     * @param string|array $config Path to json of a uri config or the config as array
     *
     * @return bool|null null when nothing was okay, true when successfully added, false when not
     */
    public function addUriBuildConfig($config) {
        return $this->uri_build->addConfig($config);
    }

    /**
     * @param string|array $routing Path to json of routing or routing as array
     *
     * @return bool|null
     */
    public function addRedirect($routing) {
        return $this->redirector->addRedirect($routing);
    }

    /**
     * This function starts MiniRoute to compare, analyse and check the active URI and
     * performs the needed executions like redirecting, cleaning and redirecting or just gives the result back
     *
     * @param bool $debug
     */
    public function route($debug = false) {

        //
        // trigger redirect evaluation and when needed do the redirect
        $this->redirect_result = $this->redirector->redirect($debug);

        // when a redirect was performed, these lines of code will never be executed as redirect exits and performs the redirect
        // but save, is save
        // as debug doesn't perform redirect, allow check when debug is on no matter if redirect should be performed
        if(!$this->getRedirectResult() || $debug) {

            $this->check_result = $this->checker->check(
                $this->active_schema,
                $this->getUriBuild(),
                $debug
            );

            if($this->check_result->getMismatch()) {
                // when the active uri is not like it should be, redirect to the one needed
                /**
                 * @var \Bemit\MiniRoute\UriSchema $new
                 */
                if(!$debug) {
                    $this->getUri()->redirect($this->check_result->getNew()->getBuildedUri(), $this->check_result->getNew()->getCode());
                }
            }
        }
    }

    /**     *
     * @return \Bemit\MiniRoute\ResultChecker
     */
    public function getCheckResult() {
        return $this->check_result;
    }

    /**
     * @return \Bemit\MiniRoute\ResultRedirector
     */
    public function getRedirectResult() {
        return $this->redirect_result;
    }
}