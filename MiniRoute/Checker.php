<?php

namespace Bemit\MiniRoute;

/**
 * Does the ckecking process
 *
 * @category
 * @package    \Bemit\MiniRoute
 * @author     Original Author mb@project.bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0
 * @version    0.1.0
 */

class Checker {

    /**
     * @var \Bemit\MiniRoute\MiniRoute
     */
    protected $mini_route;

    /**
     * @param \Bemit\MiniRoute\MiniRoute $mini_route
     */
    public function __construct(&$mini_route) {
        $this->mini_route = $mini_route;
    }

    /**
     * @param \Bemit\MiniRoute\UriSchema $uri
     * @param \Bemit\MiniRoute\UriBuild  $uri_build
     * @param bool                       $debug
     *
     * @return \Bemit\MiniRoute\ResultChecker
     */
    public function check($uri, $uri_build, $debug = false) {
        $result = new ResultChecker();

        $result->setOld($uri);

        if($uri->getSsl() !== $uri_build->getConfig('ssl', $uri->getUri()['hostname'])) {
            $result->setMismatchSsl(true);
        }

        if(
            ($uri->getWww() && false === $uri_build->getConfig('www', $uri->getUri()['hostname'])) ||
            ($uri->getWww() && true === $uri_build->getConfig('www', $uri->getUri()['hostname']) &&
                true === $uri_build->getConfig('subdomain', $uri->getUri()['hostname'])) ||
            (!$uri->getWww() && true === $uri_build->getConfig('www', $uri->getUri()['hostname']) &&
                false === $uri_build->getConfig('subdomain', $uri->getUri()['hostname']))
        ) {
            // www mismatched happen
            // because it was submitted, but config said no www
            // because it was submitted, but config said true www but also it is a subdomain (subdomain = disallow www)
            // because it was not submitted, but config said true www but also it is not a subdomain (no subdomain = allow www)
            $result->setMismatchWww(true);
        }

        if($uri->getTrailingSlash() !== $uri_build->getConfig('trailing_slash', $uri->getUri()['hostname'])) {
            $result->setMismatchTrailingSlash(true);
        }

        if($result->getMismatchSsl() || $result->getMismatchWww() || $result->getMismatchTrailingSlash()) {
            $data = [
                'uri' => [
                    'hostname' => $uri->getUri()['hostname'],
                    'path'     => $uri->getUri()['path'],
                ],
            ];
            if(!$result->getMismatchSsl()) {
                $data['ssl'] = $uri->getSsl();
            }
            if(!$result->getMismatchWww()) {
                $data['www'] = $uri->getWww();
            }
            if(!$result->getMismatchTrailingSlash()) {
                $data['trailing_slash'] = $uri->getTrailingSlash();
            }
            $result->setMismatch(true);
            $result->setNew($this->mini_route->getUriSchema()->n($data, true));
            $result->getNew()->setBuildedUri($uri_build->buildBySchema($result->getNew()));
        } else {
            $data = [
                'uri'             => [
                    'hostname' => $uri->getUri()['hostname'],
                    'path'     => $uri->getUri()['path'],
                ],
                'ssl'            => $uri->getSsl(),
                'www'            => $uri->getWww(),
                'trailing_slash' => $uri->getTrailingSlash(),
                'code'           => '200',
            ];
            $result->setNew($this->mini_route->getUriSchema()->n($data, true));
            $result->getNew()->setBuildedUri($uri_build->buildBySchema($result->getNew()));
        }

        return $result;
    }

}