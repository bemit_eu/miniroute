<?php

namespace Bemit\MiniRoute;

/**
 * Provides the need information about the URI for the route
 *
 * @category
 * @package    \Bemit\MiniRoute
 * @author     Original Author mb@project.bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0.0.2
 * @version    0.1.0
 */

class Uri {

    /**
     * Contains tagged uris
     *
     * @todo implement usage, they must be found at fetching the to information
     *
     * @var array
     */
    protected $uri_collection = [];

    /**
     * Contains the information of the active runtime ['hostname' => [], 'param' => [], 'path' => [], 'info' => ['trailing_slash' => bool, 'ssl' =>
     * bool, 'www' => bool]]
     *
     * @var array
     *
     * @todo: rewrite with UriSchema
     */
    protected $active = [];

    /**
     * Runtime path, if installed in a subdirectory, atm only one could be added, use only when one subdirectory in all paths, when not that, you
     * must add the runtime path to every config value
     *
     * @var string
     */
    protected $runtime_path = '';

    /**
     * @var \Bemit\MiniRoute\MiniRoute
     */
    protected $mini_route;

    /**
     * @param \Bemit\MiniRoute\MiniRoute $mini_route
     */
    public function __construct(&$mini_route) {
        $this->mini_route = $mini_route;
    }

    /**
     * @param string|array $collection Path to json of a uri collection or the collection as array
     *
     * @return bool|null null when nothing was okay, true when successfully added, false when not
     */
    public function addCollection($collection) {
        if(is_string($collection)) {
            return $this->addCollectionString($collection);
        } else if(is_array($collection)) {
            return $this->addCollectionArray($collection);
        } else {
            return null;
        }
    }

    /**
     * @param string $collection Path to routing file
     *
     * @return bool
     */
    protected function addCollectionString($collection) {
        $return_val = false;
        if(!is_file($collection)) {
            echo "\r\n" . 'URI-Collection could not be found: ' . $collection . "\r\n";
        }
        if(is_string($collection) && is_file($collection)) {
            $this->uri_collection = array_merge($this->uri_collection, $this->mini_route->getCache()->json($collection, true));
            $return_val = true;
        }

        return $return_val;
    }

    /**
     * @param array $collection
     *
     * @return bool
     */
    protected function addCollectionArray($collection) {
        $return_val = false;
        if(is_array($collection)) {
            $this->uri_collection = array_merge($this->uri_collection, $collection);
            $return_val = true;
        }

        return $return_val;
    }

    /**
     * @param array $active when not set, it automatically sets the information
     *
     * @return \Bemit\MiniRoute\UriSchema the schema of the active URI that was just set
     */
    public function addActive($active = []) {
        if(isset($active['hostname'])) {
            $this->setActiveHostname($active['hostname']);
        } else {
            $this->setActiveHostname(filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_URL));
        }

        if(isset($active['path-query'])) {
            $request_part = $active['path-query'];
        } else {
            $request_part = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL);
        }

        if(false === strpos($request_part, '?')) {
            //When there is no param (values) part in string
            $this->addActivePath($this->mini_route->getUriHelper()->stripSlash($request_part));
            // need to be checked not parsed path value
            if($this->mini_route->getUriHelper()->hasSlashTrailing($request_part)) {
                $this->active['info']['trailing_slash'] = true;
            } else {
                $this->active['info']['trailing_slash'] = false;
            }
        } else {
            //When there is an param (values) part in string
            $bothparts_tmp = explode('?', $request_part);// split path part from query part

            $this->addActivePath($this->mini_route->getUriHelper()->stripSlash($bothparts_tmp[0]));
            // need to be checked not parsed path value
            if($this->mini_route->getUriHelper()->hasSlashTrailing($bothparts_tmp[0])) {
                $this->active['info']['trailing_slash'] = true;
            } else {
                $this->active['info']['trailing_slash'] = false;
            }

            $tmp_query_arr = explode('&', $bothparts_tmp[1]);
            foreach($tmp_query_arr as $q_item) {
                // Go through all get value strings with key and value and make the key and the value pairs
                $this->active['param'][substr($q_item, 0, strpos($q_item, '='))] = substr($q_item, strpos($q_item, '=') + 1);
            }
        }

        // contains no string when ssl if off in apache/nginx
        // contains 'on' or 'off' on IIS
        if(!isset($_SERVER['HTTPS']) || 'on' !== $_SERVER['HTTPS']) {
            $this->setActiveSsl(false);
        } else {
            $this->setActiveSsl(true);
        }

        if('www' === substr($this->getActiveHostname(false), 0, 3)) {
            // remove www from hostname and reset
            $this->setActiveHostname(substr($this->getActiveHostname(false), 4));
            $this->active['info']['www'] = true;
        } else {
            $this->active['info']['www'] = false;
        }

        return $this->mini_route->getUriSchema()->n([
                'uri'            => [
                    'hostname' => $this->getActiveHostname(false),
                    'path'     => $this->getActivePath(false),
                ],
                'ssl'            => $this->getActive()['info']['ssl'],
                'www'            => $this->getActive()['info']['www'],
                'trailing_slash' => $this->getActive()['info']['trailing_slash'],
            ]
        );
    }

    /**
     * @param string $hostname
     */
    public function setActiveHostname($hostname) {
        $this->active['hostname'] = explode('.', $hostname);
    }

    /**
     * Only used internally for deletion of runtime_path from URI path
     *
     * @param string $path
     */
    protected function addActivePath($path) {
        if(!empty($this->runtime_path) && 0 === strpos($path, $this->runtime_path)) {
            $this->active['path'] = explode('/', $this->mini_route->getUriHelper()->stripSlash(substr($path, strlen($this->runtime_path))));
        } else {
            $this->active['path'] = explode('/', $this->mini_route->getUriHelper()->stripSlash($path));
        }
    }

    /**
     * SSL Status is detemined with $_SERVER, when not working for your implementation you can set if AFTER setActive manually with this
     *
     * @param $ssl
     */
    public function setActiveSsl($ssl) {
        $this->active['info']['ssl'] = $ssl;
    }

    /**
     * @param bool $as_array
     *
     * @return array|string
     */
    public function getActiveHostname($as_array = true) {
        return $this->returnHostname($this->active['hostname'], $as_array);
    }

    /**
     * @param array  $hostname
     * @param bool   $as_array
     * @param string $seperator
     *
     * @return string|array
     */
    protected function returnHostname($hostname, $as_array, $seperator = '.') {
        return $this->valueConcating($hostname, $as_array, $seperator);
    }

    /**
     * @param array  $value
     * @param bool   $as_array
     * @param string $seperator
     *
     * @return string|array
     */
    protected function valueConcating($value, $as_array, $seperator) {
        if(true === $as_array) {
            return $value;
        } else {
            return implode($seperator, $value);
        }
    }

    /**
     * Adds the path as runtime path, meaning when using the software like example.org/host-dir host-dir must be given as runtime path. This will be
     * deleted from the beginning of all incoming URI paths.
     *
     * It will be prepended on all redirects where no hostname was added, but not on 'domain known' redirects AND on
     *
     * @param $path
     */
    public function addRuntimePath($path) {
        $this->runtime_path = $path;
    }

    /**
     * @return string
     */
    public function getRuntimePath() {
        return $this->runtime_path;
    }

    public function getActive() {
        return $this->active;
    }

    /**
     * @param bool $as_array
     *
     * @return array|string
     */
    public function getActivePath($as_array = true) {
        return $this->returnPath($this->active['path'], $as_array);
    }

    /**
     * @param array  $path
     * @param bool   $as_array
     * @param string $seperator
     *
     * @return string|array
     */
    protected function returnPath($path, $as_array, $seperator = '/') {
        return $this->valueConcating($path, $as_array, $seperator);
    }

    /**
     *
     * @todo implement
     *
     * @param bool $as_array
     *
     * @return array|string
     */
    public function getActiveParam($as_array = true) {
        return '';
    }

    /**
     * Returns the information array off a tagged
     *
     * @param string $tag
     * @param bool   $as_array true will concat path, domain and param
     *
     * @return array|bool
     */
    public function getTagged($tag, $as_array = true) {
        if(array_key_exists($tag, $this->uri_collection)) {
            return $this->uri_collection[$tag];
        } else {
            return false;
        }
    }

    /**
     * Redirects to a uri with a header code, and adds when wanted additional headers
     *
     * @todo implement additional_header
     *
     * @param      $goto
     * @param int  $header_code
     * @param null $additional_header pre headers are added before header_code and post after ['pre' => [], 'post' => []]
     */
    public function redirect($goto, $header_code = 301, $additional_header = null) {
        $header_code_msg = $_SERVER['SERVER_PROTOCOL'] . ' ';

        $header_code_list = [
            /* Successfull Operation */
            '200' => 'OK',
            '205' => 'Reset Content',
            '206' => 'Partial Content',
            '207' => 'Multi-Status',

            /* Redirection */
            '300' => 'Multiple Choices',
            '301' => 'Moved Permanently',
            '302' => 'Found',
            '307' => 'Temporary Redirect',
            '308' => 'Permanent Redirect',

            /*  Client-Error */
            '401' => 'Unauthorized',
            '403' => 'Forbidden',
            '404' => 'Not Found',
            '408' => 'Request Time-out',
            '410' => 'Gone',
            '418' => 'I\'m a teapot',
            '421' => 'There are too many connections from your internet address',
            '423' => 'Locked',
            '424' => 'Failed Dependency',
            '429' => 'Too Many Requests',
            '451' => 'Unavailable For Legal Reasons',

            /* Server-Error */
            '503' => 'Service Unavailable',
        ];

        $header_code_msg .= $header_code;
        if(array_key_exists($header_code, $header_code_list)) {
            $header_code_msg .= ' ' . $header_code_list[$header_code];
        }

        header($header_code_msg);

        header('Location: ' . $goto);
        exit();
    }
}