<?php

namespace Bemit\MiniRoute;

/**
 * Container for the result of Checker
 *
 * @category
 * @package    \Bemit\MiniRoute
 * @author     Original Author mb@project.bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0.2.0
 * @version    0.1.0
 */

class ResultChecker {

    /**
     * @var bool
     */
    protected $mismatch;

    /**
     * @var bool
     */
    protected $mismatch_ssl;

    /**
     * @var bool
     */
    protected $mismatch_www;

    /**
     * @var bool
     */
    protected $mismatch_trailing_slash;

    /**
     * @var \Bemit\MiniRoute\UriSchema|null
     */
    protected $old;

    /**
     * @var \Bemit\MiniRoute\UriSchema|null
     */
    protected $new;

    /**
     * ResultChecker constructor.
     */
    public function __construct() {
        $this->mismatch = false;
        $this->mismatch_ssl = false;
        $this->mismatch_www = false;
        $this->mismatch_trailing_slash = false;

        $this->old = null;

        $this->new = null;
    }

    /**
     * @param $mismatch
     */
    public function setMismatch($mismatch) {
        $this->mismatch = $mismatch;
    }

    /**
     * @return bool
     */
    public function getMismatch() {
        return $this->mismatch;
    }

    /**
     * @param $mismatch
     */
    public function setMismatchSsl($mismatch) {
        $this->mismatch_ssl = $mismatch;
    }

    /**
     * @return bool
     */
    public function getMismatchSsl() {
        return $this->mismatch_ssl;
    }

    /**
     * @param $mismatch
     */
    public function setMismatchWww($mismatch) {
        $this->mismatch_www = $mismatch;
    }

    /**
     * @return bool
     */
    public function getMismatchWww() {
        return $this->mismatch_www;
    }

    /**
     * @param $mismatch
     */
    public function setMismatchTrailingSlash($mismatch) {
        $this->mismatch_trailing_slash = $mismatch;
    }

    /**
     * @return bool
     */
    public function getMismatchTrailingSlash() {
        return $this->mismatch_trailing_slash;
    }

    /**
     * @param $old
     */
    public function setOld($old) {
        $this->old = $old;
    }

    /**
     * @return \Bemit\MiniRoute\UriSchema|null
     */
    public function getOld() {
        return $this->old;
    }

    /**
     * @param $new
     */
    public function setNew($new) {
        $this->new = $new;
    }

    /**
     * @return \Bemit\MiniRoute\UriSchema|null
     */
    public function getNew() {
        return $this->new;
    }
}