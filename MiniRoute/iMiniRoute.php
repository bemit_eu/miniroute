<?php

namespace Bemit\MiniRoute;

/**
 * Saves the information about a specific uri schema
 *
 * Uri schemas define how the uri should be build and cleaned and things like that
 *
 * @category
 * @package    \Bemit\MiniRoute
 * @author     Original Author mb@project.bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0.1.0
 * @version    0.1.0
 */

interface iMiniRoute {

    /**
     * @return \Bemit\MiniRoute\Cache
     */
    public function getCache();

    /**
     * @return \Bemit\MiniRoute\Uri
     */
    public function getUri();

    /**
     * @return \Bemit\MiniRoute\UriBuild
     */
    public function getUriBuild();

    /**
     * @return \Bemit\MiniRoute\UriHelper
     */
    public function getUriHelper();

    /**
     * @return \Bemit\MiniRoute\UriSchema
     */
    public function getUriSchema();

    /**
     * @return \Bemit\MiniRoute\Redirector
     */
    public function getRedirector();

    /**
     * @param $option
     *
     * @return bool if cache was turned on or not
     */
    public function initCache($option);

    /**
     * Adds the path as runtime path, meaning when using the software like example.org/host-dir host-dir must be given as runtime path. This will be
     * deleted from the beginning of all incoming URI paths.
     *
     * It will be prepended on all redirects where no hostname was added, but not on 'domain known' redirects AND on
     *
     * @param $path
     */
    public function addRuntimePath($path);

    /**
     * Add the active Uri
     *
     * @param array $active
     */
    public function addUriActive($active);

    /**
     * @param string|array $collection Path to json of a uri collection or the collection as array
     *
     * @return bool|null null when nothing was okay, true when successfully added, false when not
     */
    public function addUriCollection($collection);

    /**
     * @param string|array $config Path to json of a uri config or the config as array
     *
     * @return bool|null null when nothing was okay, true when successfully added, false when not
     */
    public function addUriBuildConfig($config);

    /**
     * @param string|array $routing Path to json of routing or routing as array
     *
     * @return bool|null
     */
    public function addRedirect($routing);

    /**
     * @param bool $debug
     */
    public function route($debug);

}