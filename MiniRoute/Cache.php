<?php

namespace Bemit\MiniRoute;

/**
 * Simple JSON/PHP Caching
 *
 * Could be used to cache JSON files in PHP files or to just read JSON files
 *
 * @category
 * @package    \Bemit\MiniRoute
 * @author     Original Author mb@project.bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0.0.2
 * @version    0.1.0
 */

class Cache {

    /**
     * The folder of the cache containing file
     *
     * @var string
     */
    protected $path;

    /**
     * The cache containing file
     *
     * @var string
     */
    protected $file = 'miniroute-cache.php';

    /**
     * The complete path to the cache containing file
     *
     * @var string
     */
    protected $location;

    /**
     * If cache is active
     *
     * @var bool
     */
    protected $on;

    /**
     * If cache should check for updated files, when true it will update the cache when needed
     *
     * @var bool
     */
    protected $check_time;

    /**
     * The read in already cached, cache
     *
     * @var array
     */
    protected $cache = [];

    protected $id_prefix = [
        'path:',
    ];

    /**
     * @var \Bemit\MiniRoute\MiniRoute
     */
    protected $mini_route;

    public function __construct() {
    }

    /**
     * @param $option
     *
     * @return bool if cache was turned on or not
     */
    public function init($option) {
        if(isset($option['on']) && is_bool($option['on'])) {
            $this->on = $option['on'];
        } else {
            $this->on = false;
        }
        if(isset($option['file'])) {
            $this->file = $option['file'];
        }
        if(isset($option['path'])) {
            $this->path = $option['path'];
        } else {
            $this->on = false;
        }
        if(isset($option['check_time'])) {
            $this->check_time = $option['check_time'];
        } else {
            $this->on = false;
        }
        $this->readConfig();

        return $this->on;
    }

    /**
     * When cache is on this reads the already cached files
     */
    protected function readConfig() {
        if($this->on) {
            // location of cache container
            $this->location = $this->path . '/' . $this->file;
            if(!is_dir($this->path)) {
                mkdir($this->path, 0755, true);
            }
            if(is_file($this->location)) {
                $this->cache = include $this->location;
            }
        }
    }

    /**
     * Checks if the wanted file is already in cache
     *
     * @param string $filename
     * @param bool   $assoc
     * @param int    $depth
     * @param int    $options
     *
     * @return bool|mixed
     */
    public function json($filename, $assoc = false, $depth = 512, $options = 0) {
        $id = 'path:' . $filename;

        if($this->on) {
            if($this->checkExist($id) && (!$this->check_time || ($this->check_time && $this->checkUpToDate($id)))) {
                // when exists in cache AND it should check the time and if it is up to date, or no time check
                $return_val = $this->get($id);
            } else if($this->checkExist($id) && $this->check_time && !$this->checkUpToDate($id)) {
                // when not up to date
                $this->cache[$id] = [
                    'timestamp' => filemtime($filename),
                    'path'      => $filename,
                    'content'   => json_decode(file_get_contents($filename), $assoc, $depth, $options),
                ];
                $return_val = $this->get($id);
                file_put_contents($this->location, '<?php' . "\n" . 'return ' . var_export($this->cache, true) . ';');
            } else {
                $return_val = null;
            }
        } else {
            $return_val = json_decode(file_get_contents($filename), $assoc, $depth, $options);
        }

        return $return_val;
    }

    /**
     * Checks if the cache with the given id exists
     *
     * @param string|float|int $id
     *
     * @return bool
     */
    protected function checkExist($id) {
        if(isset($id, $this->cache)) {
            return true;
        }

        return false;
    }

    /**
     * If the cache is up to date with the file that was cached
     *
     * @param string|float|int $id
     *
     * @return bool
     */
    protected function checkUpToDate($id) {
        if(null !== $this->get($id, 'path')) {
            if(filemtime($this->get($id, 'path')) > $this->get($id, 'timestamp')) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    /**
     * Returns the information of a cached element
     *
     * @param        $id
     * @param string $part possible: 'content' for the cached content, 'timestamp', 'path'
     *
     * @return mixed|null
     */
    protected function get($id, $part = 'content') {
        if(isset($this->cache[$id][$part])) {
            return $this->cache[$id][$part];
        } else {
            return null;
        }
    }
}