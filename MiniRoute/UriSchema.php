<?php

namespace Bemit\MiniRoute;

/**
 * Saves the information about a specific uri schema
 *
 * Uri schemas define how the uri should be build and cleaned and things like that
 *
 * @category
 * @package    \Bemit\MiniRoute
 * @author     Original Author mb@project.bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0.0.1
 * @version    0.1.1
 */

class UriSchema {

    /**
     * @var array [ 'hostname'=>'', 'path'=>'', 'query'=>'' ]
     */
    protected $uri = [];

    /**
     * @var string the builded uri, from the schema it contains
     */
    protected $builded_uri;

    /**
     * @var bool
     */
    protected $ssl;

    /**
     * @var bool
     */
    protected $www;

    /**
     * @var bool
     */
    protected $trailing_slash;

    /**
     * @var bool
     */
    protected $subdomain;

    /**
     * @var int
     */
    protected $code;

    protected $i = [];

    /**
     * @var \Bemit\MiniRoute\MiniRoute
     */
    protected $mini_route;

    /**
     * @param \Bemit\MiniRoute\MiniRoute $mini_route
     */
    public function __construct(&$mini_route) {
        $this->mini_route = $mini_route;
    }

    /**
     * @param array|self $data
     *
     * @param bool       $fallback_config when set to true it automatically uses the config values to fill non filled fields
     *
     * @return self
     */
    public function n($data, $fallback_config = false) {
        $self = $this->i(count($this->i));

        if(isset($data['uri'])) {
            $self->setUri($data['uri']);
        }
        if(isset($data['ssl'])) {
            $self->setSsl($data['ssl']);
        } else if($fallback_config) {
            $self->setSsl($this->mini_route->getUriBuild()->getConfig('ssl', $data['uri']['hostname']));
        }
        if(isset($data['www'])) {
            $self->setWww($data['www']);
        } else if($fallback_config) {
            $self->setWww($this->mini_route->getUriBuild()->getConfig('www', $data['uri']['hostname']));
        }
        if(isset($data['trailing_slash'])) {
            $self->setTrailingSlash($data['trailing_slash']);
        } else if($fallback_config) {
            $self->setTrailingSlash($this->mini_route->getUriBuild()->getConfig('trailing_slash', $data['uri']['hostname']));
        }
        if(isset($data['code'])) {
            $self->setCode($data['code']);
        } else if($fallback_config) {
            $self->setCode($this->mini_route->getUriBuild()->getConfig('code', $data['uri']['hostname']));
        }
        if($fallback_config) {
            if(null !== $this->mini_route->getUriBuild()->getConfig('subdomain', $data['uri']['hostname'])) {
                $self->setSubdomain($this->mini_route->getUriBuild()->getConfig('subdomain', $data['uri']['hostname']));
            } else {
                // hard fallback to false, todo: subdomain general
                $self->setSubdomain(false);
            }
        }

        return $self;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function i($id = 0) {
        if(!isset($this->i[$id])) {
            $this->i[$id] = new self($this->mini_route);
        }

        return $this->i[$id];
    }

    /**
     * @param null|string $index when set it checks if the given index exists
     *
     * @return bool
     */
    public function hasUri($index = null) {
        if(null === $index) {
            if(isset($this->uri) && !empty($this->uri)) {
                return true;
            }
        } else {
            if(isset($this->uri[$index]) && !empty($this->uri[$index])) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasSsl() {
        if(isset($this->ssl) && !empty($this->ssl)) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasWww() {
        if(isset($this->www) && !empty($this->www)) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasTrailingSlash() {
        if(isset($this->trailing_slash) && !empty($this->trailing_slash)) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasSubdomain() {
        if(isset($this->subdomain) && !empty($this->subdomain)) {
            return true;
        }

        return false;
    }

    /**
     * @return int
     */
    public function hasCode() {
        if(isset($this->code) && !empty($this->code)) {
            return true;
        }

        return false;
    }

    /**
     * Array with [hostname => '', path => '']
     *
     * @return array
     */
    public function getUri() {
        return $this->uri;
    }

    /**
     * @param array $uri
     */
    public function setUri($uri) {
        $this->uri = $uri;
    }

    /**
     * @return string
     */
    public function getBuildedUri() {
        if(empty($this->builded_uri)) {
            $this->builded_uri = $this->mini_route->getUriBuild()->buildBySchema($this);
        }
        return $this->builded_uri;
    }

    /**
     * @param string $builded_uri
     */
    public function setBuildedUri($builded_uri) {
        $this->builded_uri = $builded_uri;
    }

    /**
     * @return bool
     */
    public function getSsl() {
        return $this->ssl;
    }

    /**
     * @param bool $ssl
     */
    public function setSsl($ssl) {
        $this->ssl = $ssl;
    }

    /**
     * @return bool
     */
    public function getWww() {
        return $this->www;
    }

    /**
     * @param bool $www
     */
    public function setWww($www) {
        $this->www = $www;
    }

    /**
     * @return bool
     */
    public function getTrailingSlash() {
        return $this->trailing_slash;
    }

    /**
     * @param bool $trailing_slash
     */
    public function setTrailingSlash($trailing_slash) {
        $this->trailing_slash = $trailing_slash;
    }

    /**
     * @return bool
     */
    public function getSubdomain() {
        return $this->subdomain;
    }

    /**
     * @param bool $subdomain
     */
    public function setSubdomain($subdomain) {
        $this->subdomain = $subdomain;
    }

    /**
     * @return int
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode($code) {
        $this->code = $code;
    }
}